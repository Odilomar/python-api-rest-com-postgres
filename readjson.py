import json
import psycopg2

def databaseConnection():
    return psycopg2.connect(
        user = "postgres",
        password = "postgres",
        host = "localhost",
        port = "5432",
        database = "swapi" )

def executeQuery(query):    
    result = ''
    try:
        conn = databaseConnection()
        cursor = conn.cursor()
        result = cursor.execute('SELECT COUNT(*) FROM swapi.categorypage').fetcthall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if(conn):
            cursor.close()
            conn.close()
        
    return result

def orderResultsUrl(info):
    return info['url']

def init():    
    print(executeQuery(''))
    with open('assets/swapi/responses/films.json') as json_file:
        data = json.load(json_file)
    
    data['results'].sort(key=orderResultsUrl)
    
    for results in data['results']:
        print(executeQuery("SELECT COUNT(*) FROM swapi.film WHERE title ILIKE '%{}%'".format(results['title'])))

if __name__ == '__main__':
    init()