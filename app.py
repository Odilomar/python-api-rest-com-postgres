#imports
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask_cors import CORS
from json import dumps
import psycopg2
import os

#Conect to database
# db_connect = create_engine('postgresql://postgres:postgres@localhost:5432/swapi', encoding='utf-8')
db_connect = create_engine(os.environ.get('DATABASE_URL'), encoding='utf-8')
app = Flask(__name__)
app.config.from_object(os.environ.get('APP_SETTINGS'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
api = Api(app)
CORS(app)

#Create Endpoints
class Home(Resource):
    def get(self):
        return jsonify(
            API='STAR WARS RESTFULL API - FLASK + POSTGRESQL',
            NICK = 'SWAPI',
            SEARCH = '/version/'
        )
        
class Version(Resource):
    def get(self):
        return jsonify(
            {
                'name': 'STAR WARS RESTFULL API - FLASK + POSTGRESQL', 
                'nick': 'SWAPI',
                'dev': 'Odilomar Jr.',
                'gitlab': 'gitlab.com/Odilomar' ,
                'contact': 'odilomar.junior@gmail.com',
                'version': '1.0' 
            }
        )
    
class CategoryPage(Resource):
    def get(self):
        conn = db_connect.connect()
        
        query = conn.execute("SELECT * FROM swapi.categorypage")
        result = [dict(zip(tuple(query.keys()), i)) for i in query.cursor]
        
        conn.close()
        return jsonify(result)

class FilmByPage(Resource):        
    def get(self):
        conn = db_connect.connect()
        currentPage = request.args.get('page', 1)
        searchTitle = request.args.get('title', '')
        
        if(searchTitle == '') :
            queryResult = conn.execute("SELECT swapi.getFilmPage({}::int)".format(currentPage))
        else :
            queryResult = conn.execute("SELECT swapi.searchFilmPageByTitle({}::text, {}::int)".format(searchTitle, currentPage)) 
            
           
        resultPage = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor]
        
        conn.close()
        return jsonify(resultPage)
    
class FilmById(Resource):        
    def get(self, idfilm):
        conn = db_connect.connect()     
        
        queryResult = conn.execute("SELECT swapi.getFilm({}::int)".format(idfilm))        
        resultFilm = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor]
        
        conn.close()
        return jsonify(resultFilm)
    
class PeopleByPage(Resource):
    def get(self):
        conn = db_connect.connect()
        currentPage = request.args.get('page', 1)
        searchName = request.args.get('name', '')  
        
        if (searchName == ''):
            queryResult = conn.execute("SELECT swapi.getPeoplePage({}::int)".format(currentPage))
        else:
            queryResult = conn.execute("SELECT swapi.searchPeoplePageByName({}::text, {}::int)".format(searchName, currentPage))

        resultPage = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultPage)

class PeopleById(Resource):
    def get(self, idpeople):
        conn = db_connect.connect()
        
        queryResult = conn.execute("SELECT swapi.getPeople({}::int)".format(idpeople))
        resultPeople = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 

        conn.close()    
        return jsonify(resultPeople)

class PlanetsByPage(Resource):
    def get(self):
        conn = db_connect.connect()
        currentPage = request.args.get('page', 1)
        searchName = request.args.get('name', '')  
        
        if searchName == '':
            queryResult = conn.execute("SELECT swapi.getPlanetPage({}::int)".format(currentPage))
        else:
            queryResult = conn.execute("SELECT swapi.searchPlanetPageByName({}::text, {}::int)".format(searchName, currentPage))
        
        resultPage = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultPage)

class PlanetById(Resource):
    def get(self, idplanets):
        conn = db_connect.connect()
         
        queryResult = conn.execute("SELECT swapi.getPlanet({}::int)".format(idplanets))
        resultPlanet = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultPlanet)
    
class SpeciesByPage(Resource):
    def get(self):
        conn = db_connect.connect()
        currentPage = request.args.get('page', 1)
        searchName = request.args.get('name', '') 
        
        if searchName == '':
            queryResult = conn.execute("SELECT swapi.getSpeciesPage({}::int)".format(currentPage))
        else:
            queryResult = conn.execute("SELECT swapi.searchSpeciesPageByName({}::text, {}::int)".format(searchName, currentPage))
            
        resultPage = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultPage)

class SpeciesById(Resource):
    def get(self, idspecies):
        conn = db_connect.connect()
        
        queryResult = conn.execute("SELECT swapi.getSpecies({}::int)".format(idspecies))
        resultSpecies = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultSpecies)

class StarshipByPage(Resource):
    def get(self):
        conn = db_connect.connect()
        currentPage = request.args.get('page', 1)
        searchName = request.args.get('name', '') 
        
        if searchName == '':
            queryResult = conn.execute("SELECT swapi.getStarshipPage({}::int)".format(currentPage))
        else:
            queryResult = conn.execute("SELECT swapi.searchStarshipPageByName({}::text, {}::int)".format(searchName, currentPage))
        
        resultPage = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultPage)

class StarshipById(Resource):
    def get(self, idstarships):
        conn = db_connect.connect()
        
        queryResult = conn.execute("SELECT swapi.getStarship({}::int)".format(idstarships))
        resultSpecies = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultSpecies)
    
class VehicleByPage(Resource):
    def get(self):
        conn = db_connect.connect()
        currentPage = request.args.get('page', 1)
        searchName = request.args.get('name', '') 
        
        if searchName == '':
            queryResult = conn.execute("SELECT swapi.getVehiclePage({}::int)".format(currentPage))
        else:
            queryResult = conn.execute("SELECT swapi.searchVehiclePageByName({}::text, {}::int)".format(searchName, currentPage))
        
        resultPage = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultPage)

class VehicleById(Resource):
    def get(self, idvehicles):
        conn = db_connect.connect()
        
        queryResult = conn.execute("SELECT swapi.getVehicle({}::int)".format(idvehicles))
        resultSpecies = [dict(zip(tuple(queryResult.keys()), i)) for i in queryResult.cursor] 
        
        conn.close()
        return jsonify(resultSpecies)

#Map created endpoints
api.add_resource(Home, '/')
api.add_resource(Version, '/version/')
api.add_resource(CategoryPage, '/categorypage/')
api.add_resource(FilmByPage, '/films/')
api.add_resource(FilmById, '/films/<int:idfilm>/')
api.add_resource(PeopleByPage, '/people/')
api.add_resource(PeopleById, '/people/<int:idpeople>/')
api.add_resource(PlanetsByPage, '/planets/')
api.add_resource(PlanetById, '/planets/<int:idplanets>/')
api.add_resource(SpeciesByPage, '/species/')
api.add_resource(SpeciesById, '/species/<int:idspecies>/')
api.add_resource(StarshipByPage, '/starships/')
api.add_resource(StarshipById, '/starships/<int:idstarships>/')
api.add_resource(VehicleByPage, '/vehicles/')
api.add_resource(VehicleById, '/vehicles/<int:idvehicles>/')

#Create erros handler
def page_not_found(e):
    return jsonify(API="SWAPI", status=404, message=str(e))

def page_gone(e):
    return jsonify(API="SWAPI", status=410, message=str(e))

def internal_server_error(e):
    return jsonify(API="SWAPI", status=500, message=str(e))

if __name__ == '__main__':
    app.register_error_handler(404, page_not_found)
    app.register_error_handler(410, page_gone)
    app.register_error_handler(500, internal_server_error)
    
    # app.run(host='0.0.0.0', port=80)
    app.run()