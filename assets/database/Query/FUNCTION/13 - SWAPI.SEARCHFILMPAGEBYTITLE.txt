DROP FUNCTION IF EXISTS swapi.searchFilmPageByTitle(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchFilmPageByTitle(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonFilms JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(resulQuery) INTO jsonFilms FROM (SELECT sfilm.title, sfilm.url FROM swapi.film sfilm WHERE title ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(1 - 1)) AS resulQuery;
	SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/films/?title=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/films/?title=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonFilms as results FROM swapi.film WHERE title ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

SELECT swapi.searchFilmPageByTitle('h', 1);