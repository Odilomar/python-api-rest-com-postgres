--swapi.getPeople
DROP FUNCTION IF EXISTS swapi.getPeople(integer);

CREATE OR REPLACE FUNCTION swapi.getPeople(pidpeople INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonPeople JSON;
BEGIN

	SELECT json_agg(people) INTO jsonPeople FROM (SELECT sp.name, sp.height, sp.mass, sp.hair_color, sp.skin_color, sp.eye_color, sp.birth_year, sp.gender,	(SELECT splanet.url AS homeworld FROM swapi.people_planet spp JOIN swapi.planet splanet ON splanet.idplanet = spp.idplanet WHERE spp.idpeople = pidpeople), (SELECT ARRAY(SELECT sf.url FROM swapi.film_people sfp JOIN swapi.film sf ON sf.idfilm = sfp.idfilm WHERE sfp.idpeople = pidpeople) AS films), (SELECT ARRAY(SELECT ss.url FROM swapi.people_species sps JOIN swapi.species ss ON ss.idspecies = sps.idspecies WHERE sps.idpeople = pidpeople) AS species), (SELECT ARRAY(SELECT sv.url FROM swapi.people_vehicle spv JOIN swapi.vehicle sv ON sv.idvehicle = spv.idvehicle WHERE spv.idpeople = pidpeople) AS vehicles), (SELECT ARRAY(SELECT ss.url FROM swapi.people_starship sps JOIN swapi.starship ss ON ss.idstarship = sps.idstarship WHERE sps.idpeople = pidpeople) AS starships), sp.created, sp.edited, sp.url FROM swapi.people sp WHERE sp.idpeople = pidpeople) AS people;

	RETURN jsonPeople;
END;
$$ LANGUAGE plpgsql;

--swapi.getPeoplePage

DROP FUNCTION IF EXISTS swapi.getPeoplePage(integer);

CREATE OR REPLACE FUNCTION swapi.getPeoplePage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
	pCategoryPage INTEGER := 2;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(peopleQuery) FROM (SELECT speople.name, speople.url FROM swapi.page_results spr JOIN swapi.people speople ON speople.idpeople = spr.idpeople WHERE spr.idpage = spage.idpage) AS peopleQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;

	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

--swapi.getFilm

DROP FUNCTION IF EXISTS swapi.getFilm(integer);

CREATE OR REPLACE FUNCTION swapi.getFilm(pidfilm INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonFilm JSON;
BEGIN

    SELECT json_agg(film) INTO jsonFilm FROM (SELECT sf.title, sf.episode_id, sf.opening_crawl, sf.director, sf.producer, release_date, sf.created, sf.edited, sf.url, (SELECT ARRAY(SELECT sp.url FROM swapi.film_people sfp JOIN swapi.people sp ON sp.idpeople = sfp.idpeople WHERE sfp.idfilm = pidfilm) AS characters), (SELECT ARRAY(SELECT ss.url FROM swapi.film_species sfs JOIN swapi.species ss ON ss.idspecies = sfs.idspecies WHERE sfs.idfilm = pidfilm) AS species), (SELECT ARRAY(SELECT ss.url FROM swapi.film_starship sfs JOIN swapi.starship ss ON ss.idstarship = sfs.idstarship WHERE sfs.idfilm = pidfilm) AS starships), (SELECT ARRAY(SELECT sv.url FROM swapi.film_vehicle sfv JOIN swapi.vehicle sv ON sv.idvehicle = sfv.idvehicle WHERE sfv.idfilm = pidfilm) AS vehicles), (SELECT ARRAY(SELECT sp.url FROM swapi.films_planets sfp JOIN swapi.planet sp ON sp.idplanet = sfp.idplanet WHERE sfp.idfilm = pidfilm) AS planets) FROM swapi.film sf WHERE sf.idfilm = pidfilm) AS film;

	RETURN jsonFilm;
END;
$$ LANGUAGE plpgsql;

--swapi.getFilmPage

DROP FUNCTION IF EXISTS swapi.getFilmPage(integer);

CREATE OR REPLACE FUNCTION swapi.getFilmPage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 1;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(filmQuery) FROM (SELECT sfilm.title, sfilm.url FROM swapi.page_results spr JOIN swapi.film sfilm ON sfilm.idfilm = spr.idfilm WHERE spr.idpage = spage.idpage) AS filmQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;


--swapi.getPlanet
DROP FUNCTION IF EXISTS swapi.getPlanet(integer);

CREATE OR REPLACE FUNCTION swapi.getPlanet(pidPlanet INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonPlanet JSON;
BEGIN

    SELECT json_agg(planet) INTO jsonPlanet FROM ( SELECT splanet.name, splanet.rotation_period, splanet.orbital_period, splanet.diameter, splanet.climate, splanet.gravity, splanet.terrain, splanet.surface_water, splanet.population, (SELECT ARRAY(SELECT sp.url FROM swapi.people_planet spp JOIN swapi.people sp ON sp.idpeople = spp.idpeople WHERE spp.idplanet = pidPlanet) AS residents), (SELECT ARRAY(SELECT sf.url FROM swapi.films_planets sfp JOIN swapi.film sf ON sf.idfilm = sfp.idfilm WHERE sfp.idplanet = pidPlanet) AS films), splanet.created, splanet.edited, splanet.url FROM swapi.planet splanet WHERE splanet.idplanet = pidPlanet ) AS planet;

	RETURN jsonPlanet;
END;
$$ LANGUAGE plpgsql;

--swapi.getPlanetPage

DROP FUNCTION IF EXISTS swapi.getPlanetPage(integer);

CREATE OR REPLACE FUNCTION swapi.getPlanetPage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 3;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(planetQuery) FROM (SELECT splanet.name, splanet.url FROM swapi.page_results spr JOIN swapi.planet splanet ON splanet.idplanet = spr.idplanet WHERE spr.idpage = spage.idpage) AS planetQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;	
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

--swapi.getSpecies

DROP FUNCTION IF EXISTS swapi.getSpecies(integer);

CREATE OR REPLACE FUNCTION swapi.getSpecies(pidSpecies INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonSpecies JSON;
BEGIN

    SELECT json_agg(species) INTO jsonSpecies FROM ( SELECT sspecies.name, sspecies.classification, sspecies.designation, sspecies.average_height, sspecies.skin_color, sspecies.hair_color, sspecies.eye_color, sspecies.average_lifespan, (SELECT ARRAY(SELECT sp.url FROM swapi.planet_species spp JOIN swapi.planet sp ON sp.idplanet = spp.idplanet WHERE spp.idspecies = pidSpecies) AS homeworld), sspecies.language, (SELECT ARRAY(SELECT sp.url FROM swapi.people_species sps JOIN swapi.people sp ON sp.idpeople = sps.idpeople WHERE sps.idspecies = pidSpecies) AS people), (SELECT ARRAY(SELECT sf.url FROM swapi.film_species sfs JOIN swapi.film sf ON sf.idfilm = sfs.idfilm WHERE sfs.idspecies = pidSpecies) AS films), sspecies.created, sspecies.edited, sspecies.url FROM swapi.species sspecies WHERE sspecies.idspecies = pidSpecies) AS species;

	RETURN jsonSpecies;
END;
$$ LANGUAGE plpgsql;

--swapi.getSpeciesPage
DROP FUNCTION IF EXISTS swapi.getSpeciesPage(integer);

CREATE OR REPLACE FUNCTION swapi.getSpeciesPage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 4;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(speciesQuery) FROM (SELECT sspecies.name, sspecies.url FROM swapi.page_results spr JOIN swapi.species sspecies ON sspecies.idspecies = spr.idspecies WHERE spr.idpage = spage.idpage) AS speciesQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;	
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;
--swapi.getStarship
DROP FUNCTION IF EXISTS swapi.getStarship(integer);

CREATE OR REPLACE FUNCTION swapi.getStarship(pidStarship INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonStarship JSON;
BEGIN

    SELECT json_agg(starship) INTO jsonStarship FROM ( SELECT ss.name, ss.model, ss.manufacturer, ss.cost_in_credits, ss.length, ss.max_atmosphering_speed, ss.crew, ss.passengers, ss.cargo_capacity, ss.consumables, ss.hyperdrive_rating, ss.MGLT, ss.starship_class, (SELECT ARRAY(SELECT sp.url FROM swapi.people_starship sps JOIN swapi.people sp ON sp.idpeople = sps.idpeople WHERE sps.idstarship = pidStarship) AS pilots), (SELECT ARRAY(SELECT sf.url FROM swapi.film_starship sfs JOIN swapi.film sf ON sf.idfilm = sfs.idfilm WHERE sfs.idstarship = pidStarship) AS films),ss.created, ss.edited, ss.url FROM swapi.starship ss WHERE ss.idstarship = pidStarship) AS starship;

	RETURN jsonStarship;
END;
$$ LANGUAGE plpgsql;

--swapi.getStarshipPage
DROP FUNCTION IF EXISTS swapi.getStarshipPage(integer);

CREATE OR REPLACE FUNCTION swapi.getStarshipPage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 5;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(starshipQuery) FROM (SELECT sstarship.name, sstarship.url FROM swapi.page_results spr JOIN swapi.starship sstarship ON sstarship.idstarship = spr.idstarship WHERE spr.idpage = spage.idpage) AS starshipQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;

	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

--swapi.getVehicle
DROP FUNCTION IF EXISTS swapi.getVehicle(integer);

CREATE OR REPLACE FUNCTION swapi.getVehicle(pidVehicle INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonVehicle JSON;
BEGIN

    SELECT json_agg(vehicle) INTO jsonVehicle FROM ( SELECT sv.name, sv.model, sv.manufacturer, sv.cost_in_credits, sv.length, sv.max_atmosphering_speed, sv.crew, sv.passengers, sv.cargo_capacity, sv.consumables, sv.vehicle_class, (SELECT ARRAY(SELECT sp.url FROM swapi.people_vehicle spv JOIN swapi.people sp ON sp.idpeople = spv.idpeople WHERE spv.idvehicle = pidVehicle) AS pilots), (SELECT ARRAY(SELECT sf.url FROM swapi.film_vehicle sfv JOIN swapi.film sf ON sf.idfilm = sfv.idfilm WHERE sfv.idvehicle = pidVehicle) AS films),sv.created, sv.edited, sv.url FROM swapi.vehicle sv WHERE sv.idvehicle = pidVehicle) AS vehicle;

	RETURN jsonVehicle;
END;
$$ LANGUAGE plpgsql;

--swapi.getVehiclePage
DROP FUNCTION IF EXISTS swapi.getVehiclePage(integer);

CREATE OR REPLACE FUNCTION swapi.getVehiclePage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 6;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(vehicleQuery) FROM (SELECT svehicle.name, svehicle.url FROM swapi.page_results spr JOIN swapi.vehicle svehicle ON svehicle.idvehicle = spr.idvehicle WHERE spr.idpage = spage.idpage) AS vehicleQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;

	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS swapi.searchFilmPageByTitle(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchFilmPageByTitle(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonFilms JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(resulQuery) INTO jsonFilms FROM (SELECT sfilm.title, sfilm.url FROM swapi.film sfilm WHERE title ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(1 - 1)) AS resulQuery;
	SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/films/?title=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/films/?title=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonFilms as results FROM swapi.film WHERE title ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS swapi.searchPeoplePageByName(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchPeoplePageByName(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonPeople JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(peopleQuery) INTO jsonPeople FROM (SELECT speople.name, speople.url FROM swapi.people speople WHERE name ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(ppage - 1)) AS peopleQuery;
    SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/people/?name=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/people/?name=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonPeople as results FROM swapi.people WHERE name ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS swapi.searchPlanetPageByName(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchPlanetPageByName(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonPlanet JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(planetQuery) INTO jsonPlanet FROM (SELECT sp.name, sp.url FROM swapi.planet sp WHERE sp.name ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(ppage - 1)) AS planetQuery;
    SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/planets/?name=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/planets/?name=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonPlanet as results FROM swapi.planet WHERE name ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS swapi.searchSpeciesPageByName(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchSpeciesPageByName(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonSpecies JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(speciesQuery) INTO jsonSpecies FROM (SELECT ss.name, ss.url FROM swapi.species ss WHERE ss.name ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(ppage - 1)) AS speciesQuery;
    SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/species/?name=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/species/?name=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonSpecies as results FROM swapi.species WHERE name ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS swapi.searchStarshipPageByName(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchStarshipPageByName(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonStarship JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(starshipQuery) INTO jsonStarship FROM (SELECT ss.name, ss.url FROM swapi.starship ss WHERE ss.name ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(ppage - 1)) AS starshipQuery;
    SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/starships/?name=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/starships/?name=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonStarship as results FROM swapi.starship WHERE name ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS swapi.searchVehiclePageByName(TEXT, INTEGER);

CREATE OR REPLACE FUNCTION swapi.searchVehiclePageByName(ptxtName TEXT, ppage INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonVehicle JSON := '{}';
	jsonResult JSON := '{}';
BEGIN
	SELECT json_agg(vehicleQuery) INTO jsonVehicle FROM (SELECT sv.name, sv.url FROM swapi.vehicle sv WHERE sv.name ILIKE '%' || ptxtName || '%' LIMIT 10 OFFSET 10*(ppage - 1)) AS vehicleQuery;
    SELECT json_agg(searchpage) INTO jsonResult FROM (SELECT count(*) AS count, (CASE WHEN ppage = 1 THEN 'null' ELSE 'http://localhost:80/vechiles/?name=''' || ptxtName || '''&page=' || ppage - 1 || '' END) AS previous, (CASE WHEN CEIL(count(*) / 10.0) > ppage THEN 'http://localhost:80/vechiles/?name=''' || ptxtName || '''&page=' || ppage + 1 || '' ELSE 'null' END) AS next, jsonVehicle as results FROM swapi.vehicle WHERE name ILIKE '%' || ptxtName || '%') AS searchpage;
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;