DROP FUNCTION IF EXISTS swapi.getVehiclePage(integer);

CREATE OR REPLACE FUNCTION swapi.getVehiclePage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 6;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(vehicleQuery) FROM (SELECT svehicle.name, svehicle.url FROM swapi.page_results spr JOIN swapi.vehicle svehicle ON svehicle.idvehicle = spr.idvehicle WHERE spr.idpage = spage.idpage) AS vehicleQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;

	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

SELECT swapi.getVehiclePage(1);