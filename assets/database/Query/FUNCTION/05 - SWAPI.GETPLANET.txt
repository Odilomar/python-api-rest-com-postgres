DROP FUNCTION IF EXISTS swapi.getPlanet(integer);

CREATE OR REPLACE FUNCTION swapi.getPlanet(pidPlanet INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonPlanet JSON;
BEGIN

    SELECT json_agg(planet) INTO jsonPlanet FROM ( SELECT splanet.name, splanet.rotation_period, splanet.orbital_period, splanet.diameter, splanet.climate, splanet.gravity, splanet.terrain, splanet.surface_water, splanet.population, (SELECT ARRAY(SELECT sp.url FROM swapi.people_planet spp JOIN swapi.people sp ON sp.idpeople = spp.idpeople WHERE spp.idplanet = pidPlanet) AS residents), (SELECT ARRAY(SELECT sf.url FROM swapi.films_planets sfp JOIN swapi.film sf ON sf.idfilm = sfp.idfilm WHERE sfp.idplanet = pidPlanet) AS films), splanet.created, splanet.edited, splanet.url FROM swapi.planet splanet WHERE splanet.idplanet = pidPlanet ) AS planet;

	RETURN jsonPlanet;
END;
$$ LANGUAGE plpgsql;

SELECT swapi.getPlanet(2);