DROP FUNCTION IF EXISTS swapi.getPlanetPage(integer);

CREATE OR REPLACE FUNCTION swapi.getPlanetPage(currentPageInput INTEGER)
RETURNS JSON AS $$
DECLARE 
	jsonResult JSON := '{}';
    pCategoryPage INTEGER := 3;
BEGIN

	SELECT json_agg(pageQuery) INTO jsonResult FROM (SELECT spage.count, spage.next, spage.previous, (SELECT json_agg(planetQuery) FROM (SELECT splanet.name, splanet.url FROM swapi.page_results spr JOIN swapi.planet splanet ON splanet.idplanet = spr.idplanet WHERE spr.idpage = spage.idpage) AS planetQuery) AS results FROM swapi.page spage WHERE spage.idcategorypage = pCategoryPage AND spage.currentpage = currentPageInput) AS pageQuery;	
	
	RETURN jsonResult;
END;
$$ LANGUAGE plpgsql;

SELECT swapi.getPlanetPage(1);