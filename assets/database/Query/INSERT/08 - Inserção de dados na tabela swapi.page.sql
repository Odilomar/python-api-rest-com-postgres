-- Film
INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    1,
    7, 
    1,
    'null', 
    'null'
);

-- People
INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    1,
    'https://swapi.co/api/people/?page=2', 
    'null'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    2,
    'https://swapi.co/api/people/?page=3', 
    'https://swapi.co/api/people/?page=1'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    3,
    'https://swapi.co/api/people/?page=4', 
    'https://swapi.co/api/people/?page=2'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    4,
    'https://swapi.co/api/people/?page=5', 
    'https://swapi.co/api/people/?page=3'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    5,
    'https://swapi.co/api/people/?page=6', 
    'https://swapi.co/api/people/?page=4'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    6,
    'https://swapi.co/api/people/?page=7',
    'https://swapi.co/api/people/?page=5'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    7,
    'https://swapi.co/api/people/?page=8', 
    'https://swapi.co/api/people/?page=6'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    8,
    'https://swapi.co/api/people/?page=9', 
    'https://swapi.co/api/people/?page=7'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    2,
    87, 
    9,
    'null', 
    'https://swapi.co/api/people/?page=8'
);


-- Planets
INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    1,
    'https://swapi.co/api/planets/?page=2', 
    'null'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    2,
    'https://swapi.co/api/planets/?page=3', 
    'https://swapi.co/api/planets/?page=1'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    3,
    'https://swapi.co/api/planets/?page=4', 
    'https://swapi.co/api/planets/?page=2'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    4,
    'https://swapi.co/api/planets/?page=5', 
    'https://swapi.co/api/planets/?page=3'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    5,
    'https://swapi.co/api/planets/?page=6', 
    'https://swapi.co/api/planets/?page=4'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    6,
    'https://swapi.co/api/planets/?page=7', 
    'https://swapi.co/api/planets/?page=5'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    3,
    61, 
    7,
    'null', 
    'https://swapi.co/api/planets/?page=6'
);


-- Species
INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    4,
    37, 
    1,
    'https://swapi.co/api/species/?page=2', 
    'null'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    4,
    37, 
    2,
    'https://swapi.co/api/species/?page=3', 
    'https://swapi.co/api/species/?page=1'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    4,
    37, 
    3,
    'https://swapi.co/api/species/?page=4', 
    'https://swapi.co/api/species/?page=2'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    4,
    37, 
    4,
    'null', 
    'https://swapi.co/api/species/?page=3'
);

-- Starships
INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    5,
    37, 
    1,
    'https://swapi.co/api/starships/?page=2', 
    'null'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    5,
    37, 
    2,
    'https://swapi.co/api/starships/?page=3', 
    'https://swapi.co/api/starships/?page=1'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    5,
    37, 
    3,
    'https://swapi.co/api/starships/?page=4', 
    'https://swapi.co/api/starships/?page=2'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    5,
    37, 
    4,
    'null', 
    'https://swapi.co/api/starships/?page=3'
);

-- Vehicles
INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    6,
    39, 
    1,
    'https://swapi.co/api/vehicles/?page=2', 
    'null'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    6,
    39, 
    2,
    'https://swapi.co/api/vehicles/?page=3', 
    'https://swapi.co/api/vehicles/?page=1'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    6,
    39, 
    3,
    'https://swapi.co/api/vehicles/?page=4', 
    'https://swapi.co/api/vehicles/?page=2'
);

INSERT INTO swapi.page
(idcategorypage, count, currentPage, next, previous)
VALUES
(
    6,
    39, 
    4,
    'null', 
    'https://swapi.co/api/vehicles/?page=3'
);

