INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/1/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/2/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/3/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Sand Crawler', 
    'Digger Crawler', 
    'Corellia Mining Corporation', 
    '150000', 
    '36.8', 
    '30', 
    '46', 
    '30', 
    '50000', 
    '2 months', 
    'wheeled', 
    '2014-12-10T15:36:25.724000Z', 
    '2014-12-22T18:21:15.523587Z', 
    'https://swapi.co/api/vehicles/4/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/5/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'T-16 skyhopper', 
    'T-16 skyhopper', 
    'Incom Corporation', 
    '14500', 
    '10.4', 
    '1200', 
    '1', 
    '1', 
    '50', 
    '0', 
    'repulsorcraft', 
    '2014-12-10T16:01:52.434000Z', 
    '2014-12-22T18:21:15.552614Z', 
    'https://swapi.co/api/vehicles/6/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'X-34 landspeeder', 
    'X-34 landspeeder', 
    'SoroSuub Corporation', 
    '10550', 
    '3.4', 
    '250', 
    '1', 
    '1', 
    '5', 
    'unknown', 
    'repulsorcraft', 
    '2014-12-10T16:13:52.586000Z', 
    '2014-12-22T18:21:15.583700Z', 
    'https://swapi.co/api/vehicles/7/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'TIE/LN starfighter', 
    'Twin Ion Engine/Ln Starfighter', 
    'Sienar Fleet Systems', 
    'unknown', 
    '6.4', 
    '1200', 
    '1', 
    '0', 
    '65', 
    '2 days', 
    'starfighter', 
    '2014-12-10T16:33:52.860000Z', 
    '2014-12-22T18:21:15.606149Z', 
    'https://swapi.co/api/vehicles/8/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/9/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/10/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/11/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/12/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/13/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Snowspeeder', 
    't-47 airspeeder', 
    'Incom corporation', 
    'unknown', 
    '4.5', 
    '650', 
    '2', 
    '0', 
    '10', 
    'none', 
    'airspeeder', 
    '2014-12-15T12:22:12Z', 
    '2014-12-22T18:21:15.623033Z', 
    'https://swapi.co/api/vehicles/14/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/15/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'TIE bomber', 
    'TIE/sa bomber', 
    'Sienar Fleet Systems', 
    'unknown', 
    '7.8', 
    '850', 
    '1', 
    '0', 
    'none', 
    '2 days', 
    'space/planetary bomber', 
    '2014-12-15T12:33:15.838000Z', 
    '2014-12-22T18:21:15.667730Z', 
    'https://swapi.co/api/vehicles/16/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/17/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'AT-AT', 
    'All Terrain Armored Transport', 
    'Kuat Drive Yards, Imperial Department of Military Research', 
    'unknown', 
    '20', 
    '60', 
    '5', 
    '40', 
    '1000', 
    'unknown', 
    'assault walker', 
    '2014-12-15T12:38:25.937000Z', 
    '2014-12-22T18:21:15.714673Z', 
    'https://swapi.co/api/vehicles/18/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'AT-ST', 
    'All Terrain Scout Transport', 
    'Kuat Drive Yards, Imperial Department of Military Research', 
    'unknown', 
    '2', 
    '90', 
    '2', 
    '0', 
    '200', 
    'none', 
    'walker', 
    '2014-12-15T12:46:42.384000Z', 
    '2014-12-22T18:21:15.761584Z', 
    'https://swapi.co/api/vehicles/19/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Storm IV Twin-Pod cloud car', 
    'Storm IV Twin-Pod', 
    'Bespin Motors', 
    '75000', 
    '7', 
    '1500', 
    '2', 
    '0', 
    '10', 
    '1 day', 
    'repulsorcraft', 
    '2014-12-15T12:58:50.530000Z', 
    '2014-12-22T18:21:15.783232Z', 
    'https://swapi.co/api/vehicles/20/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/21/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/22/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/23/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Sail barge', 
    'Modified Luxury Sail Barge', 
    'Ubrikkian Industries Custom Vehicle Division', 
    '285000', 
    '30', 
    '100', 
    '26', 
    '500', 
    '2000000', 
    'Live food tanks', 
    'Sail barge', 
    '2014-12-18T10:44:14.217000Z', 
    '2014-12-22T18:21:15.807906Z', 
    'https://swapi.co/api/vehicles/24/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Bantha-II cargo skiff', 
    'Bantha-II', 
    'Ubrikkian Industries', 
    '8000', 
    '9.5', 
    '250', 
    '5', 
    '16', 
    '135000', 
    '1 day', 
    'repulsorcraft cargo skiff', 
    '2014-12-18T10:48:03.208000Z', 
    '2014-12-22T18:21:15.845988Z', 
    'https://swapi.co/api/vehicles/25/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'TIE/IN interceptor', 
    'Twin Ion Engine Interceptor', 
    'Sienar Fleet Systems', 
    'unknown', 
    '9.6', 
    '1250', 
    '1', 
    '0', 
    '75', 
    '2 days', 
    'starfighter', 
    '2014-12-18T10:50:28.225000Z', 
    '2014-12-22T18:21:15.882388Z', 
    'https://swapi.co/api/vehicles/26/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/27/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/28/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/29/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Imperial Speeder Bike', 
    '74-Z speeder bike', 
    'Aratech Repulsor Company', 
    '8000', 
    '3', 
    '360', 
    '1', 
    '1', 
    '4', 
    '1 day', 
    'speeder', 
    '2014-12-18T11:20:04.625000Z', 
    '2014-12-22T18:21:15.920537Z', 
    'https://swapi.co/api/vehicles/30/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/31/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/32/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Vulture Droid', 
    'Vulture-class droid starfighter', 
    'Haor Chall Engineering, Baktoid Armor Workshop', 
    'unknown', 
    '3.5', 
    '1200', 
    '0', 
    '0', 
    '0', 
    'none', 
    'starfighter', 
    '2014-12-19T17:09:53.584000Z', 
    '2014-12-22T18:21:15.953870Z', 
    'https://swapi.co/api/vehicles/33/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Multi-Troop Transport', 
    'Multi-Troop Transport', 
    'Baktoid Armor Workshop', 
    '138000', 
    '31', 
    '35', 
    '4', 
    '112', 
    '12000', 
    'unknown', 
    'repulsorcraft', 
    '2014-12-19T17:12:04.400000Z', 
    '2014-12-22T18:21:15.975171Z', 
    'https://swapi.co/api/vehicles/34/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Armored Assault Tank', 
    'Armoured Assault Tank', 
    'Baktoid Armor Workshop', 
    'unknown', 
    '9.75', 
    '55', 
    '4', 
    '6', 
    'unknown', 
    'unknown', 
    'repulsorcraft', 
    '2014-12-19T17:13:29.799000Z', 
    '2014-12-22T18:21:15.984817Z', 
    'https://swapi.co/api/vehicles/35/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Single Trooper Aerial Platform', 
    'Single Trooper Aerial Platform', 
    'Baktoid Armor Workshop', 
    '2500', 
    '2', 
    '400', 
    '1', 
    '0', 
    'none', 
    'none', 
    'repulsorcraft', 
    '2014-12-19T17:15:09.511000Z', 
    '2014-12-22T18:21:16.008594Z', 
    'https://swapi.co/api/vehicles/36/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'C-9979 landing craft', 
    'C-9979 landing craft', 
    'Haor Chall Engineering', 
    '200000', 
    '210', 
    '587', 
    '140', 
    '284', 
    '1800000', 
    '1 day', 
    'landing craft', 
    '2014-12-19T17:20:36.373000Z', 
    '2014-12-22T18:21:16.033738Z', 
    'https://swapi.co/api/vehicles/37/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Tribubble bongo', 
    'Tribubble bongo', 
    'Otoh Gunga Bongameken Cooperative', 
    'unknown', 
    '15', 
    '85', 
    '1', 
    '2', 
    '1600', 
    'unknown', 
    'submarine', 
    '2014-12-19T17:37:37.924000Z', 
    '2014-12-22T18:21:16.072083Z', 
    'https://swapi.co/api/vehicles/38/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/39/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/40/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/41/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Sith speeder', 
    'FC-20 speeder bike', 
    'Razalon', 
    '4000', 
    '1.5', 
    '180', 
    '1', 
    '0', 
    '2', 
    'unknown', 
    'speeder', 
    '2014-12-20T10:09:56.095000Z', 
    '2014-12-22T18:21:16.095041Z', 
    'https://swapi.co/api/vehicles/42/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/43/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Zephyr-G swoop bike', 
    'Zephyr-G swoop bike', 
    'Mobquet Swoops and Speeders', 
    '5750', 
    '3.68', 
    '350', 
    '1', 
    '1', 
    '200', 
    'none', 
    'repulsorcraft', 
    '2014-12-20T16:24:16.026000Z', 
    '2014-12-22T18:21:16.117652Z', 
    'https://swapi.co/api/vehicles/44/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Koro-2 Exodrive airspeeder', 
    'Koro-2 Exodrive airspeeder', 
    'Desler Gizh Outworld Mobility Corporation', 
    'unknown', 
    '6.6', 
    '800', 
    '1', 
    '1', 
    '80', 
    'unknown', 
    'airspeeder', 
    '2014-12-20T17:17:33.526000Z', 
    '2014-12-22T18:21:16.140018Z', 
    'https://swapi.co/api/vehicles/45/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'XJ-6 airspeeder', 
    'XJ-6 airspeeder', 
    'Narglatch AirTech prefabricated kit', 
    'unknown', 
    '6.23', 
    '720', 
    '1', 
    '1', 
    'unknown', 
    'unknown', 
    'airspeeder', 
    '2014-12-20T17:19:19.991000Z', 
    '2014-12-22T18:21:16.150194Z', 
    'https://swapi.co/api/vehicles/46/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/47/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/48/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/49/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'LAAT/i', 
    'Low Altitude Assault Transport/infrantry', 
    'Rothana Heavy Engineering', 
    'unknown', 
    '17.4', 
    '620', 
    '6', 
    '30', 
    '170', 
    'unknown', 
    'gunship', 
    '2014-12-20T18:01:21.014000Z', 
    '2014-12-22T18:21:16.181363Z', 
    'https://swapi.co/api/vehicles/50/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'LAAT/c', 
    'Low Altitude Assault Transport/carrier', 
    'Rothana Heavy Engineering', 
    'unknown', 
    '28.82', 
    '620', 
    '1', 
    '0', 
    '40000', 
    'unknown', 
    'gunship', 
    '2014-12-20T18:02:46.802000Z', 
    '2014-12-22T18:21:16.229733Z', 
    'https://swapi.co/api/vehicles/51/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/52/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'AT-TE', 
    'All Terrain Tactical Enforcer', 
    'Rothana Heavy Engineering, Kuat Drive Yards', 
    'unknown', 
    '13.2', 
    '60', 
    '6', 
    '36', 
    '10000', 
    '21 days', 
    'walker', 
    '2014-12-20T18:10:07.560000Z', 
    '2014-12-22T18:21:16.293771Z', 
    'https://swapi.co/api/vehicles/53/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'SPHA', 
    'Self-Propelled Heavy Artillery', 
    'Rothana Heavy Engineering', 
    'unknown', 
    '140', 
    '35', 
    '25', 
    '30', 
    '500', 
    '7 days', 
    'walker', 
    '2014-12-20T18:12:32.315000Z', 
    '2014-12-22T18:21:16.311761Z', 
    'https://swapi.co/api/vehicles/54/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Flitknot speeder', 
    'Flitknot speeder', 
    'Huppla Pasa Tisc Shipwrights Collective', 
    '8000', 
    '2', 
    '634', 
    '1', 
    '0', 
    'unknown', 
    'unknown', 
    'speeder', 
    '2014-12-20T18:15:20.312000Z', 
    '2014-12-22T18:21:16.335005Z', 
    'https://swapi.co/api/vehicles/55/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Neimoidian shuttle', 
    'Sheathipede-class transport shuttle', 
    'Haor Chall Engineering', 
    'unknown', 
    '20', 
    '880', 
    '2', 
    '6', 
    '1000', 
    '7 days', 
    'transport', 
    '2014-12-20T18:25:44.912000Z', 
    '2014-12-22T18:21:16.366134Z', 
    'https://swapi.co/api/vehicles/56/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Geonosian starfighter', 
    'Nantex-class territorial defense', 
    'Huppla Pasa Tisc Shipwrights Collective', 
    'unknown', 
    '9.8', 
    '20000', 
    '1', 
    '0', 
    'unknown', 
    'unknown', 
    'starfighter', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/57/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/58/'
);


INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/59/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Tsmeu-6 personal wheel bike', 
    'Tsmeu-6 personal wheel bike', 
    'Z-Gomot Ternbuell Guppat Corporation', 
    '15000', 
    '3.5', 
    '330', 
    '1', 
    '1', 
    '10', 
    'none', 
    'wheeled walker', 
    '2014-12-20T19:43:54.870000Z', 
    '2014-12-22T18:21:16.422662Z', 
    'https://swapi.co/api/vehicles/60/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/61/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Emergency Firespeeder', 
    'Fire suppression speeder', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2', 
    'unknown', 
    'unknown', 
    'unknown', 
    'fire suppression ship', 
    '2014-12-20T19:50:58.559000Z', 
    '2014-12-22T18:21:16.450655Z', 
    'https://swapi.co/api/vehicles/62/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/63/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/64/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/65/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/66/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Droid tri-fighter', 
    'tri-fighter', 
    'Colla Designs, Phlac-Arphocc Automata Industries', 
    '20000', 
    '5.4', 
    '1180', 
    '1', 
    '0', 
    '0', 
    'none', 
    'droid starfighter', 
    '2014-12-20T20:05:19.992000Z', 
    '2014-12-22T18:21:16.478901Z', 
    'https://swapi.co/api/vehicles/67/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/68/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Oevvaor jet catamaran', 
    'Oevvaor jet catamaran', 
    'Appazanna Engineering Works', 
    '12125', 
    '15.1', 
    '420', 
    '2', 
    '2', 
    '50', 
    '3 days', 
    'airspeeder', 
    '2014-12-20T20:20:53.931000Z', 
    '2014-12-22T18:21:16.517049Z', 
    'https://swapi.co/api/vehicles/69/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Raddaugh Gnasp fluttercraft', 
    'Raddaugh Gnasp fluttercraft', 
    'Appazanna Engineering Works', 
    '14750', 
    '7', 
    '310', 
    '2', 
    '0', 
    '20', 
    'none', 
    'air speeder', 
    '2014-12-20T20:21:55.648000Z', 
    '2014-12-22T18:21:16.547708Z', 
    'https://swapi.co/api/vehicles/70/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Clone turbo tank', 
    'HAVw A6 Juggernaut', 
    'Kuat Drive Yards', 
    '350000', 
    '49.4', 
    '160', 
    '20', 
    '300', 
    '30000', 
    '20 days', 
    'wheeled walker', 
    '2014-12-20T20:24:45.587000Z', 
    '2014-12-22T18:21:16.571079Z', 
    'https://swapi.co/api/vehicles/71/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Corporate Alliance tank droid', 
    'NR-N99 Persuader-class droid enforcer', 
    'Techno Union', 
    '49000', 
    '10.96', 
    '100', 
    '0', 
    '4', 
    'none', 
    'none', 
    'droid tank', 
    '2014-12-20T20:26:55.522000Z', 
    '2014-12-22T18:21:16.612902Z', 
    'https://swapi.co/api/vehicles/72/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'Droid gunship', 
    'HMP droid gunship', 
    'Baktoid Fleet Ordnance, Haor Chall Engineering', 
    '60000', 
    '12.3', 
    '820', 
    '0', 
    '0', 
    '0', 
    'none', 
    'airspeeder', 
    '2014-12-20T20:32:05.687000Z', 
    '2014-12-22T18:21:16.643329Z', 
    'https://swapi.co/api/vehicles/73/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/74/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-20T18:34:12.541000Z', 
    '2014-12-22T18:21:16.390980Z', 
    'https://swapi.co/api/vehicles/75/'
);

INSERT INTO swapi.vehicle
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, vehicle_class, created, edited, url)
VALUES
(
    'AT-RT', 
    'All Terrain Recon Transport', 
    'Kuat Drive Yards', 
    '40000', 
    '3.2', 
    '90', 
    '1', 
    '0', 
    '20', 
    '1 day', 
    'walker', 
    '2014-12-20T20:47:49.189000Z', 
    '2014-12-22T18:21:16.672821Z', 
    'https://swapi.co/api/vehicles/76/'
);