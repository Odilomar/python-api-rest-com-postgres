INSERT INTO swapi.categorypage VALUES (
	(SELECT COUNT(*) + 1 FROM swapi.categorypage),
	'films'
);

INSERT INTO swapi.categorypage VALUES (
	(SELECT COUNT(*) + 1 FROM swapi.categorypage),
	'people'
);

INSERT INTO swapi.categorypage VALUES (
	(SELECT COUNT(*) + 1 FROM swapi.categorypage),
	'planets'
);

INSERT INTO swapi.categorypage VALUES (
	(SELECT COUNT(*) + 1 FROM swapi.categorypage),
	'starships'
);

INSERT INTO swapi.categorypage VALUES (
	(SELECT COUNT(*) + 1 FROM swapi.categorypage),
	'species'
);

INSERT INTO swapi.categorypage VALUES (
	(SELECT COUNT(*) + 1 FROM swapi.categorypage),
	'vehicles'
);