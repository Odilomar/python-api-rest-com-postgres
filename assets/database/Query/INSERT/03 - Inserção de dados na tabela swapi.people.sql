--TRUNCATE TABLE swapi.people RESTART IDENTITY CASCADE;

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Luke Skywalker',
    '172',
    '77',
    'blond',
    'fair',
    'blue',
    '19BBY',
    'male',
    '2014-12-09T13:50:51.644000Z',
    '2014-12-20T21:17:56.891000Z',
    'https://swapi.co/api/people/1/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'C-3PO',
    '167',
    '75',
    'n/a',
    'gold',
    'yellow',
    '112BBY',
    'n/a',
    '2014-12-10T15:10:51.357000Z',
    '2014-12-20T21:17:50.309000Z',
    'https://swapi.co/api/people/2/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'R2-D2',
    '96',
    '32',
    'n/a',
    'white, blue',
    'red',
    '33BBY',
    'n/a',
    '2014-12-10T15:11:50.376000Z',
    '2014-12-20T21:17:50.311000Z',
    'https://swapi.co/api/people/3/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Darth Vader',
    '202',
    '136',
    'none',
    'white',
    'yellow',
    '41.9BBY',
    'male',
    '2014-12-10T15:18:20.704000Z',
    '2014-12-20T21:17:50.313000Z',
    'https://swapi.co/api/people/4/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Leia Organa',
    '150',
    '49',
    'brown',
    'light',
    'brown',
    '19BBY',
    'female',
    '2014-12-10T15:20:09.791000Z',
    '2014-12-20T21:17:50.315000Z',
    'https://swapi.co/api/people/5/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Owen Lars',
    '178',
    '120',
    'brown, grey',
    'light',
    'blue',
    '52BBY',
    'male',
    '2014-12-10T15:52:14.024000Z',
    '2014-12-20T21:17:50.317000Z',
    'https://swapi.co/api/people/6/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Beru Whitesun lars',
    '165',
    '75',
    'brown',
    'light',
    'blue',
    '47BBY',
    'female',
    '2014-12-10T15:53:41.121000Z',
    '2014-12-20T21:17:50.319000Z',
    'https://swapi.co/api/people/7/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'R5-D4',
    '97',
    '32',
    'n/a',
    'white, red',
    'red',
    'unknown',
    'n/a',
    '2014-12-10T15:57:50.959000Z',
    '2014-12-20T21:17:50.321000Z',
    'https://swapi.co/api/people/8/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Biggs Darklighter',
    '183',
    '84',
    'black',
    'light',
    'brown',
    '24BBY',
    'male',
    '2014-12-10T15:59:50.509000Z',
    '2014-12-20T21:17:50.323000Z',
    'https://swapi.co/api/people/9/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Obi-Wan Kenobi',
    '182',
    '77',
    'auburn, white',
    'fair',
    'blue-gray',
    '57BBY',
    'male',
    '2014-12-10T16:16:29.192000Z',
    '2014-12-20T21:17:50.325000Z',
    'https://swapi.co/api/people/10/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Anakin Skywalker',
    '188',
    '84',
    'blond',
    'fair',
    'blue',
    '41.9BBY',
    'male',
    '2014-12-10T16:20:44.310000Z',
    '2014-12-20T21:17:50.327000Z',
    'https://swapi.co/api/people/11/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Wilhuff Tarkin',
    '180',
    'unknown',
    'auburn, grey',
    'fair',
    'blue',
    '64BBY',
    'male',
    '2014-12-10T16:26:56.138000Z',
    '2014-12-20T21:17:50.330000Z',
    'https://swapi.co/api/people/12/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Chewbacca',
    '228',
    '112',
    'brown',
    'unknown',
    'blue',
    '200BBY',
    'male',
    '2014-12-10T16:42:45.066000Z',
    '2014-12-20T21:17:50.332000Z',
    'https://swapi.co/api/people/13/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Han Solo',
    '180',
    '80',
    'brown',
    'fair',
    'brown',
    '29BBY',
    'male',
    '2014-12-10T16:49:14.582000Z',
    '2014-12-20T21:17:50.334000Z',
    'https://swapi.co/api/people/14/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Greedo',
    '173',
    '74',
    'n/a',
    'green',
    'black',
    '44BBY',
    'male',
    '2014-12-10T17:03:30.334000Z',
    '2014-12-20T21:17:50.336000Z',
    'https://swapi.co/api/people/15/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Jabba Desilijic Tiure',
    '175',
    '1,358',
    'n/a',
    'green-tan, brown',
    'orange',
    '600BBY',
    'hermaphrodite',
    '2014-12-10T17:11:31.638000Z',
    '2014-12-20T21:17:50.338000Z',
    'https://swapi.co/api/people/16/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    '000',
    '000',
    '000',
    '000',
    '000',
    '000',
    '000',
    '000',
    '2014-12-12T11:08:06.469000Z',
    '2014-12-20T21:17:50.341000Z',
    'https://swapi.co/api/people/17/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Wedge Antilles',
    '170',
    '77',
    'brown',
    'fair',
    'hazel',
    '21BBY',
    'male',
    '2014-12-12T11:08:06.469000Z',
    '2014-12-20T21:17:50.341000Z',
    'https://swapi.co/api/people/18/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Jek Tono Porkins',
    '180',
    '110',
    'brown',
    'fair',
    'blue',
    'unknown',
    'male',
    '2014-12-12T11:16:56.569000Z',
    '2014-12-20T21:17:50.343000Z',
    'https://swapi.co/api/people/19/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Yoda',
    '66',
    '17',
    'white',
    'green',
    'brown',
    '896BBY',
    'male',
    '2014-12-15T12:26:01.042000Z',
    '2014-12-20T21:17:50.345000Z',
    'https://swapi.co/api/people/20/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Palpatine',
    '170',
    '75',
    'grey',
    'pale',
    'yellow',
    '82BBY',
    'male',
    '2014-12-15T12:48:05.971000Z',
    '2014-12-20T21:17:50.347000Z',
    'https://swapi.co/api/people/21/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Boba Fett',
    '183',
    '78.2',
    'black',
    'fair',
    'brown',
    '31.5BBY',
    'male',
    '2014-12-15T12:49:32.457000Z',
    '2014-12-20T21:17:50.349000Z',
    'https://swapi.co/api/people/22/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'IG-88',
    '200',
    '140',
    'none',
    'metal',
    'red',
    '15BBY',
    'none',
    '2014-12-15T12:51:10.076000Z',
    '2014-12-20T21:17:50.351000Z',
    'https://swapi.co/api/people/23/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Bossk',
    '190',
    '113',
    'none',
    'green',
    'red',
    '53BBY',
    'male',
    '2014-12-15T12:53:49.297000Z',
    '2014-12-20T21:17:50.355000Z',
    'https://swapi.co/api/people/24/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Lando Calrissian',
    '177',
    '79',
    'black',
    'dark',
    'brown',
    '31BBY',
    'male',
    '2014-12-15T12:56:32.683000Z',
    '2014-12-20T21:17:50.357000Z',
    'https://swapi.co/api/people/25/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Lobot',
    '175',
    '79',
    'none',
    'light',
    'blue',
    '37BBY',
    'male',
    '2014-12-15T13:01:57.178000Z',
    '2014-12-20T21:17:50.359000Z',
    'https://swapi.co/api/people/26/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Ackbar',
    '180',
    '83',
    'none',
    'brown mottle',
    'orange',
    '41BBY',
    'male',
    '2014-12-18T11:07:50.584000Z',
    '2014-12-20T21:17:50.362000Z',
    'https://swapi.co/api/people/27/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Mon Mothma',
    '150',
    'unknown',
    'auburn',
    'fair',
    'blue',
    '48BBY',
    'female',
    '2014-12-18T11:12:38.895000Z',
    '2014-12-20T21:17:50.364000Z',
    'https://swapi.co/api/people/28/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Arvel Crynyd',
    'unknown',
    'unknown',
    'brown',
    'fair',
    'brown',
    'unknown',
    'male',
    '2014-12-18T11:16:33.020000Z',
    '2014-12-20T21:17:50.367000Z',
    'https://swapi.co/api/people/29/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Wicket Systri Warrick',
    '88',
    '20',
    'brown',
    'brown',
    'brown',
    '8BBY',
    'male',
    '2014-12-18T11:21:58.954000Z',
    '2014-12-20T21:17:50.369000Z',
    'https://swapi.co/api/people/30/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Nien Nunb',
    '160',
    '68',
    'none',
    'grey',
    'black',
    'unknown',
    'male',
    '2014-12-18T11:26:18.541000Z',
    '2014-12-20T21:17:50.371000Z',
    'https://swapi.co/api/people/31/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Qui-Gon Jinn',
    '193',
    '89',
    'brown',
    'fair',
    'blue',
    '92BBY',
    'male',
    '2014-12-19T16:54:53.618000Z',
    '2014-12-20T21:17:50.375000Z',
    'https://swapi.co/api/people/32/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Nute Gunray',
    '191',
    '90',
    'none',
    'mottled green',
    'red',
    'unknown',
    'male',
    '2014-12-19T17:05:57.357000Z',
    '2014-12-20T21:17:50.377000Z',
    'https://swapi.co/api/people/33/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Finis Valorum',
    '170',
    'unknown',
    'blond',
    'fair',
    'blue',
    '91BBY',
    'male',
    '2014-12-19T17:21:45.915000Z',
    '2014-12-20T21:17:50.379000Z',
    'https://swapi.co/api/people/34/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Padmé Amidala',
    '165',
    '45',
    'brown',
    'light',
    'brown',
    '46BBY',
    'female',
    '2014-12-19T17:28:26.926000Z',
    '2016-04-20T17:06:31.502555Z',
    'https://swapi.co/api/people/35/'
);


INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Jar Jar Binks',
    '196',
    '66',
    'none',
    'orange',
    'orange',
    '52BBY',
    'male',
    '2014-12-19T17:29:32.489000Z',
    '2014-12-20T21:17:50.383000Z',
    'https://swapi.co/api/people/36/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Roos Tarpals',
    '224',
    '82',
    'none',
    'grey',
    'orange',
    'unknown',
    'male',
    '2014-12-19T17:32:56.741000Z',
    '2014-12-20T21:17:50.385000Z',
    'https://swapi.co/api/people/37/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Rugor Nass',
    '206',
    'unknown',
    'none',
    'green',
    'orange',
    'unknown',
    'male',
    '2014-12-19T17:33:38.909000Z',
    '2014-12-20T21:17:50.388000Z',
    'https://swapi.co/api/people/38/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Ric Olié',
    '183',
    'unknown',
    'brown',
    'fair',
    'blue',
    'unknown',
    'male',
    '2014-12-19T17:45:01.522000Z',
    '2014-12-20T21:17:50.392000Z',
    'https://swapi.co/api/people/39/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Watto',
    '137',
    'unknown',
    'black',
    'blue, grey',
    'yellow',
    'unknown',
    'male',
    '2014-12-19T17:48:54.647000Z',
    '2014-12-20T21:17:50.395000Z',
    'https://swapi.co/api/people/40/'
);
INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Sebulba',
    '112',
    '40',
    'none',
    'grey, red',
    'orange',
    'unknown',
    'male',
    '2014-12-19T17:53:02.586000Z',
    '2014-12-20T21:17:50.397000Z',
    'https://swapi.co/api/people/41/'
);
INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Quarsh Panaka',
    '183',
    'unknown',
    'black',
    'dark',
    'brown',
    '62BBY',
    'male',
    '2014-12-19T17:55:43.348000Z',
    '2014-12-20T21:17:50.399000Z',
    'https://swapi.co/api/people/42/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Shmi Skywalker',
    '163',
    'unknown',
    'black',
    'fair',
    'brown',
    '72BBY',
    'female',
    '2014-12-19T17:57:41.191000Z',
    '2014-12-20T21:17:50.401000Z',
    'https://swapi.co/api/people/43/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Darth Maul',
    '175',
    '80',
    'none',
    'red',
    'yellow',
    '54BBY',
    'male',
    '2014-12-19T18:00:41.929000Z',
    '2014-12-20T21:17:50.403000Z',
    'https://swapi.co/api/people/44/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Bib Fortuna',
    '180',
    'unknown',
    'none',
    'pale',
    'pink',
    'unknown',
    'male',
    '2014-12-20T09:47:02.512000Z',
    '2014-12-20T21:17:50.407000Z',
    'https://swapi.co/api/people/45/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Ayla Secura',
    '178',
    '55',
    'none',
    'blue',
    'hazel',
    '48BBY',
    'female',
    '2014-12-20T09:48:01.172000Z',
    '2014-12-20T21:17:50.409000Z',
    'https://swapi.co/api/people/46/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Ratts Tyerell',
    '79',
    '15',
    'none',
    'grey, blue',
    'unknown',
    'unknown',
    'male',
    '2014-12-20T09:53:15.086000Z',
    '2016-06-30T12:52:19.604868Z',
    'https://swapi.co/api/people/47/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Dud Bolt',
    '94',
    '45',
    'none',
    'blue, grey',
    'yellow',
    'unknown',
    'male',
    '2014-12-20T09:57:31.858000Z',
    '2014-12-20T21:17:50.414000Z',
    'https://swapi.co/api/people/48/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Gasgano',
    '122',
    'unknown',
    'none',
    'white, blue',
    'black',
    'unknown',
    'male',
    '2014-12-20T10:02:12.223000Z',
    '2014-12-20T21:17:50.416000Z',
    'https://swapi.co/api/people/49/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Ben Quadinaros',
    '163',
    '65',
    'none',
    'grey, green, yellow',
    'orange',
    'unknown',
    'male',
    '2014-12-20T10:08:33.777000Z',
    '2014-12-20T21:17:50.417000Z',
    'https://swapi.co/api/people/50/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Mace Windu',
    '188',
    '84',
    'none',
    'dark',
    'brown',
    '72BBY',
    'male',
    '2014-12-20T10:12:30.846000Z',
    '2014-12-20T21:17:50.420000Z',
    'https://swapi.co/api/people/51/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Ki-Adi-Mundi',
    '198',
    '82',
    'white',
    'pale',
    'yellow',
    '92BBY',
    'male',
    '2014-12-20T10:15:32.293000Z',
    '2014-12-20T21:17:50.422000Z',
    'https://swapi.co/api/people/52/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Kit Fisto',
    '196',
    '87',
    'none',
    'green',
    'black',
    'unknown',
    'male',
    '2014-12-20T10:18:57.202000Z',
    '2014-12-20T21:17:50.424000Z',
    'https://swapi.co/api/people/53/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Eeth Koth',
    '171',
    'unknown',
    'black',
    'brown',
    'brown',
    'unknown',
    'male',
    '2014-12-20T10:26:47.902000Z',
    '2014-12-20T21:17:50.427000Z',
    'https://swapi.co/api/people/54/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Adi Gallia',
    '184',
    '50',
    'none',
    'dark',
    'blue',
    'unknown',
    'female',
    '2014-12-20T10:29:11.661000Z',
    '2014-12-20T21:17:50.432000Z',
    'https://swapi.co/api/people/55/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Saesee Tiin',
    '188',
    'unknown',
    'none',
    'pale',
    'orange',
    'unknown',
    'male',
    '2014-12-20T10:32:11.669000Z',
    '2014-12-20T21:17:50.434000Z',
    'https://swapi.co/api/people/56/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Yarael Poof',
    '264',
    'unknown',
    'none',
    'white',
    'yellow',
    'unknown',
    'male',
    '2014-12-20T10:34:48.725000Z',
    '2014-12-20T21:17:50.437000Z',
    'https://swapi.co/api/people/57/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Plo Koon',
    '188',
    '80',
    'none',
    'orange',
    'black',
    '22BBY',
    'male',
    '2014-12-20T10:49:19.859000Z',
    '2014-12-20T21:17:50.439000Z',
    'https://swapi.co/api/people/58/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Mas Amedda',
    '196',
    'unknown',
    'none',
    'blue',
    'blue',
    'unknown',
    'male',
    '2014-12-20T10:53:26.457000Z',
    '2014-12-20T21:17:50.442000Z',
    'https://swapi.co/api/people/59/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Gregar Typho',
    '185',
    '85',
    'black',
    'dark',
    'brown',
    'unknown',
    'male',
    '2014-12-20T11:10:10.381000Z',
    '2014-12-20T21:17:50.445000Z',
    'https://swapi.co/api/people/60/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Cordé',
    '157',
    'unknown',
    'brown',
    'light',
    'brown',
    'unknown',
    'female',
    '2014-12-20T11:11:39.630000Z',
    '2014-12-20T21:17:50.449000Z',
    'https://swapi.co/api/people/61/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Cliegg Lars',
    '183',
    'unknown',
    'brown',
    'fair',
    'blue',
    '82BBY',
    'male',
    '2014-12-20T15:59:03.958000Z',
    '2014-12-20T21:17:50.451000Z',
    'https://swapi.co/api/people/62/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Poggle the Lesser',
    '183',
    '80',
    'none',
    'green',
    'yellow',
    'unknown',
    'male',
    '2014-12-20T16:40:43.977000Z',
    '2014-12-20T21:17:50.453000Z',
    'https://swapi.co/api/people/63/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Luminara Unduli',
    '170',
    '56.2',
    'black',
    'yellow',
    'blue',
    '58BBY',
    'female',
    '2014-12-20T16:45:53.668000Z',
    '2014-12-20T21:17:50.455000Z',
    'https://swapi.co/api/people/64/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Barriss Offee',
    '166',
    '50',
    'black',
    'yellow',
    'blue',
    '40BBY',
    'female',
    '2014-12-20T16:46:40.440000Z',
    '2014-12-20T21:17:50.457000Z',
    'https://swapi.co/api/people/65/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Dormé',
    '165',
    'unknown',
    'brown',
    'light',
    'brown',
    'unknown',
    'female',
    '2014-12-20T16:49:14.640000Z',
    '2014-12-20T21:17:50.460000Z',
    'https://swapi.co/api/people/66/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Dooku',
    '193',
    '80',
    'white',
    'fair',
    'brown',
    '102BBY',
    'male',
    '2014-12-20T16:52:14.726000Z',
    '2014-12-20T21:17:50.462000Z',
    'https://swapi.co/api/people/67/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Bail Prestor Organa',
    '191',
    'unknown',
    'black',
    'tan',
    'brown',
    '67BBY',
    'male',
    '2014-12-20T16:53:08.575000Z',
    '2014-12-20T21:17:50.463000Z',
    'https://swapi.co/api/people/68/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Jango Fett',
    '183',
    '79',
    'black',
    'tan',
    'brown',
    '66BBY',
    'male',
    '2014-12-20T16:54:41.620000Z',
    '2014-12-20T21:17:50.465000Z',
    'https://swapi.co/api/people/69/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Zam Wesell',
    '168',
    '55',
    'blonde',
    'fair, green, yellow',
    'yellow',
    'unknown',
    'female',
    '2014-12-20T16:57:44.471000Z',
    '2014-12-20T21:17:50.468000Z',
    'https://swapi.co/api/people/70/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Dexter Jettster',
    '198',
    '102',
    'none',
    'brown',
    'yellow',
    'unknown',
    'male',
    '2014-12-20T17:28:27.248000Z',
    '2014-12-20T21:17:50.470000Z',
    'https://swapi.co/api/people/71/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Lama Su',
    '229',
    '88',
    'none',
    'grey',
    'black',
    'unknown',
    'male',
    '2014-12-20T17:30:50.416000Z',
    '2014-12-20T21:17:50.473000Z',
    'https://swapi.co/api/people/72/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Taun We',
    '213',
    'unknown',
    'none',
    'grey',
    'black',
    'unknown',
    'female',
    '2014-12-20T17:31:21.195000Z',
    '2014-12-20T21:17:50.474000Z',
    'https://swapi.co/api/people/73/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Jocasta Nu',
    '167',
    'unknown',
    'white',
    'fair',
    'blue',
    'unknown',
    'female',
    '2014-12-20T17:32:51.996000Z',
    '2014-12-20T21:17:50.476000Z',
    'https://swapi.co/api/people/74/'
);



INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'R4-P17',
    '96',
    'unknown',
    'none',
    'silver, red',
    'red, blue',
    'unknown',
    'female',
    '2014-12-20T17:43:36.409000Z',
    '2014-12-20T21:17:50.478000Z',
    'https://swapi.co/api/people/75/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Wat Tambor',
    '193',
    '48',
    'none',
    'green, grey',
    'unknown',
    'unknown',
    'male',
    '2014-12-20T17:53:52.607000Z',
    '2014-12-20T21:17:50.481000Z',
    'https://swapi.co/api/people/76/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'San Hill',
    '191',
    'unknown',
    'none',
    'grey',
    'gold',
    'unknown',
    'male',
    '2014-12-20T17:58:17.049000Z',
    '2014-12-20T21:17:50.484000Z',
    'https://swapi.co/api/people/77/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Shaak Ti',
    '178',
    '57',
    'none',
    'red, blue, white',
    'black',
    'unknown',
    'female',
    '2014-12-20T18:44:01.103000Z',
    '2014-12-20T21:17:50.486000Z',
    'https://swapi.co/api/people/78/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Grievous',
    '216',
    '159',
    'none',
    'brown, white',
    'green, yellow',
    'unknown',
    'male',
    '2014-12-20T19:43:53.348000Z',
    '2014-12-20T21:17:50.488000Z',
    'https://swapi.co/api/people/79/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Tarfful',
    '234',
    '136',
    'brown',
    'brown',
    'blue',
    'unknown',
    'male',
    '2014-12-20T19:46:34.209000Z',
    '2014-12-20T21:17:50.491000Z',
    'https://swapi.co/api/people/80/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Raymus Antilles',
    '188',
    '79',
    'brown',
    'light',
    'brown',
    'unknown',
    'male',
    '2014-12-20T19:49:35.583000Z',
    '2014-12-20T21:17:50.493000Z',
    'https://swapi.co/api/people/81/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Sly Moore',
    '178',
    '48',
    'none',
    'pale',
    'white',
    'unknown',
    'female',
    '2014-12-20T20:18:37.619000Z',
    '2014-12-20T21:17:50.496000Z',
    'https://swapi.co/api/people/82/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Tion Medon',
    '206',
    '80',
    'none',
    'grey',
    'black',
    'unknown',
    'male',
    '2014-12-20T20:35:04.260000Z',
    '2014-12-20T21:17:50.498000Z',
    'https://swapi.co/api/people/83/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Finn',
    'unknown',
    'unknown',
    'black',
    'dark',
    'dark',
    'unknown',
    'male',
    '2015-04-17T06:52:40.793621Z',
    '2015-04-17T06:52:40.793674Z',
    'https://swapi.co/api/people/84/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Rey',
    'unknown',
    'unknown',
    'brown',
    'light',
    'hazel',
    'unknown',
    'female',
    '2015-04-17T06:54:01.495077Z',
    '2015-04-17T06:54:01.495128Z',
    'https://swapi.co/api/people/85/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Poe Dameron',
    'unknown',
    'unknown',
    'brown',
    'light',
    'brown',
    'unknown',
    'male',
    '2015-04-17T06:55:21.622786Z',
    '2015-04-17T06:55:21.622835Z',
    'https://swapi.co/api/people/86/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'BB8',
    'unknown',
    'unknown',
    'none',
    'none',
    'black',
    'unknown',
    'none',
    '2015-04-17T06:57:38.061346Z',
    '2015-04-17T06:57:38.061453Z',
    'https://swapi.co/api/people/87/'
);

INSERT INTO swapi.people
(name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, created, edited, url)
VALUES (
    'Captain Phasma',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'female',
    '2015-10-13T10:35:39.229823Z',
    '2015-10-13T10:35:39.229894Z',
    'https://swapi.co/api/people/88/'
);
