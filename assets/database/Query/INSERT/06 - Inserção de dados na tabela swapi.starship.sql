INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/1/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'CR90 corvette', 
    'CR90 corvette', 
    'Corellian Engineering Corporation', 
    '3500000', 
    '150', 
    '950', 
    '165', 
    '600', 
    '3000000', 
    '1 year',
    '2.0', 
    '60', 
    'corvette', 
    '2014-12-10T14:20:33.369000Z', 
    '2014-12-22T17:35:45.408368Z', 
    'https://swapi.co/api/starships/2/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Star Destroyer', 
    'Imperial I-class Star Destroyer', 
    'Kuat Drive Yards', 
    '150000000', 
    '1,600', 
    '975', 
    '47060', 
    '0', 
    '36000000', 
    '2 years',
    '2.0', 
    '60', 
    'Star Destroyer', 
    '2014-12-10T15:08:19.848000Z', 
    '2014-12-22T17:35:44.410941Z', 
    'https://swapi.co/api/starships/3/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/4/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Sentinel-class landing craft', 
    'Sentinel-class landing craft', 
    'Sienar Fleet Systems, Cyngus Spaceworks', 
    '240000', 
    '38', 
    '1000', 
    '5', 
    '75', 
    '180000', 
    '1 month',
    '1.0', 
    '70', 
    'landing craft', 
    '2014-12-10T15:48:00.586000Z', 
    '2014-12-22T17:35:44.431407Z', 
    'https://swapi.co/api/starships/5/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/6/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/7/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/8/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Death Star', 
    'DS-1 Orbital Battle Station', 
    'Imperial Department of Military Research, Sienar Fleet Systems', 
    '1000000000000', 
    '120000', 
    'n/a', 
    '342953', 
    '843342', 
    '1000000000000', 
    '3 years',
    '4.0', 
    '10', 
    'Deep Space Mobile Battlestation', 
    '2014-12-10T16:36:50.509000Z', 
    '2014-12-22T17:35:44.452589Z', 
    'https://swapi.co/api/starships/9/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Millennium Falcon', 
    'YT-1300 light freighter', 
    'Corellian Engineering Corporation', 
    '100000', 
    '34.37', 
    '1050', 
    '4', 
    '6', 
    '100000', 
    '2 months',
    '0.5', 
    '75', 
    'Light freighter', 
    '2014-12-10T16:59:45.094000Z', 
    '2014-12-22T17:35:44.464156Z', 
    'https://swapi.co/api/starships/10/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Y-wing', 
    'BTL Y-wing', 
    'Koensayr Manufacturing', 
    '134999', 
    '14', 
    '1000km', 
    '2', 
    '0', 
    '110', 
    '1 week',
    '1.0', 
    '80', 
    'assault starfighter', 
    '2014-12-12T11:00:39.817000Z', 
    '2014-12-22T17:35:44.479706Z', 
    'https://swapi.co/api/starships/11/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'X-wing', 
    'T-65 X-wing', 
    'Incom Corporation', 
    '149999', 
    '12.5', 
    '1050', 
    '1', 
    '0', 
    '110', 
    '1 week',
    '1.0', 
    '100', 
    'Starfighter', 
    '2014-12-12T11:19:05.340000Z', 
    '2014-12-22T17:35:44.491233Z', 
    'https://swapi.co/api/starships/12/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'TIE Advanced x1', 
    'Twin Ion Engine Advanced x1', 
    'Sienar Fleet Systems', 
    'unknown', 
    '9.2', 
    '1200', 
    '1', 
    '0', 
    '150', 
    '5 days',
    '1.0', 
    '105', 
    'Starfighter', 
    '2014-12-12T11:21:32.991000Z', 
    '2014-12-22T17:35:44.549047Z', 
    'https://swapi.co/api/starships/13/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/14/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Executor', 
    'Executor-class star dreadnought', 
    'Kuat Drive Yards, Fondor Shipyards', 
    '1143350000', 
    '19000', 
    'n/a', 
    '279144', 
    '38000', 
    '250000000', 
    '6 years', 
    '2.0', 
    '40',
    'Star dreadnought', 
    '2014-12-15T12:31:42.547000Z', 
    '2017-04-19T10:56:06.685592Z', 
    'https://swapi.co/api/starships/15/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/16/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Rebel transport', 
    'GR-75 medium transport', 
    'Gallofree Yards, Inc.', 
    'unknown', 
    '90', 
    '650', 
    '6', 
    '90', 
    '19000000', 
    '6 months',
    '4.0', 
    '20', 
    'Medium transport', 
    '2014-12-15T12:34:52.264000Z', 
    '2014-12-22T17:35:44.680838Z', 
    'https://swapi.co/api/starships/17/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/18/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/19/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/20/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Slave 1', 
    'Firespray-31-class patrol and attack', 
    'Kuat Systems Engineering', 
    'unknown', 
    '21.5', 
    '1000', 
    '1', 
    '6', 
    '70000', 
    '1 month',
    '3.0', 
    '70', 
    'Patrol craft', 
    '2014-12-15T13:00:56.332000Z', 
    '2014-12-22T17:35:44.716273Z', 
    'https://swapi.co/api/starships/21/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Imperial shuttle', 
    'Lambda-class T-4a shuttle', 
    'Sienar Fleet Systems', 
    '240000', 
    '20', 
    '850', 
    '6', 
    '20', 
    '80000', 
    '2 months',
    '1.0', 
    '50', 
    'Armed government transport', 
    '2014-12-15T13:04:47.235000Z', 
    '2014-12-22T17:35:44.795405Z', 
    'https://swapi.co/api/starships/22/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'EF76 Nebulon-B escort frigate', 
    'EF76 Nebulon-B escort frigate', 
    'Kuat Drive Yards', 
    '8500000', 
    '300', 
    '800', 
    '854', 
    '75', 
    '6000000', 
    '2 years',
    '2.0', 
    '40', 
    'Escort ship', 
    '2014-12-15T13:06:30.813000Z', 
    '2014-12-22T17:35:44.848329Z', 
    'https://swapi.co/api/starships/23/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/24/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/25/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/26/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Calamari Cruiser', 
    'MC80 Liberty type Star Cruiser', 
    'Mon Calamari shipyards', 
    '104000000', 
    '1200', 
    'n/a', 
    '5400', 
    '1200', 
    'unknown',
    '2 years', 
    '1.0',
    '60', 
    'Star Cruiser', 
    '2014-12-18T10:54:57.804000Z', 
    '2014-12-22T17:35:44.957852Z', 
    'https://swapi.co/api/starships/27/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'A-wing', 
    'RZ-1 A-wing Interceptor', 
    'Alliance Underground Engineering, Incom Corporation', 
    '175000', 
    '9.6', 
    '1300', 
    '1', 
    '0', 
    '40', 
    '1 week',
    '1.0', 
    '120', 
    'Starfighter', 
    '2014-12-18T11:16:34.542000Z', 
    '2014-12-22T17:35:44.978754Z', 
    'https://swapi.co/api/starships/28/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'B-wing', 
    'A/SF-01 B-wing starfighter', 
    'Slayn & Korpil', 
    '220000', 
    '16.9', 
    '950', 
    '1', 
    '0', 
    '45', 
    '1 week',
    '2.0', 
    '91', 
    'Assault Starfighter', 
    '2014-12-18T11:18:04.763000Z', 
    '2014-12-22T17:35:45.011193Z', 
    'https://swapi.co/api/starships/29/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/30/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Republic Cruiser', 
    'Consular-class cruiser', 
    'Corellian Engineering Corporation', 
    'unknown', 
    '115', 
    '900', 
    '9', 
    '16', 
    'unknown', 
    'unknown',
    '2.0', 
    'unknown', 
    'Space cruiser', 
    '2014-12-19T17:01:31.488000Z', 
    '2014-12-22T17:35:45.027308Z', 
    'https://swapi.co/api/starships/31/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Droid control ship', 
    'Lucrehulk-class Droid Control Ship', 
    'Hoersch-Kessel Drive, Inc.', 
    'unknown', 
    '3170', 
    'n/a', 
    '175', 
    '139000', 
    '4000000000', 
    '500 days',
    '2.0', 
    'unknown', 
    'Droid control ship', 
    '2014-12-19T17:04:06.323000Z', 
    '2014-12-22T17:35:45.042900Z', 
    'https://swapi.co/api/starships/32/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/33/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/34/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/35/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/36/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/37/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/38/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Naboo fighter', 
    'N-1 starfighter', 
    'Theed Palace Space Vessel Engineering Corps', 
    '200000', 
    '11', 
    '1100', 
    '1', 
    '0', 
    '65', 
    '7 days',
    '1.0', 
    'unknown', 
    'Starfighter', 
    '2014-12-19T17:39:17.582000Z', 
    '2014-12-22T17:35:45.079452Z', 
    'https://swapi.co/api/starships/39/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Naboo Royal Starship', 
    'J-type 327 Nubian royal starship', 
    'Theed Palace Space Vessel Engineering Corps, Nubia Star Drives', 
    'unknown', 
    '76', 
    '920', 
    '8', 
    'unknown', 
    'unknown', 
    'unknown',
    '1.8', 
    'unknown', 
    'yacht', 
    '2014-12-19T17:45:03.506000Z', 
    '2014-12-22T17:35:45.091925Z', 
    'https://swapi.co/api/starships/40/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Scimitar', 
    'Star Courier', 
    'Republic Sienar Systems', 
    '55000000', 
    '26.5', 
    '1180', 
    '1', 
    '6', 
    '2500000', 
    '30 days',
    '1.5', 
    'unknown', 
    'Space Transport', 
    '2014-12-20T09:39:56.116000Z', 
    '2014-12-22T17:35:45.105522Z', 
    'https://swapi.co/api/starships/41/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/42/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'J-type diplomatic barge', 
    'J-type diplomatic barge', 
    'Theed Palace Space Vessel Engineering Corps, Nubia Star Drives', 
    '2000000', 
    '39', 
    '2000', 
    '5', 
    '10', 
    'unknown', 
    '1 year',
    '0.7', 
    'unknown', 
    'Diplomatic barge', 
    '2014-12-20T11:05:51.237000Z', 
    '2014-12-22T17:35:45.124386Z', 
    'https://swapi.co/api/starships/43/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/44/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/45/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/46/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'AA-9 Coruscant freighter', 
    'Botajef AA-9 Freighter-Liner', 
    'Botajef Shipyards', 
    'unknown', 
    '390', 
    'unknown', 
    'unknown', 
    '30000', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'freighter', 
    '2014-12-20T17:24:23.509000Z', 
    '2014-12-22T17:35:45.135987Z', 
    'https://swapi.co/api/starships/47/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Jedi starfighter', 
    'Delta-7 Aethersprite-class interceptor', 
    'Kuat Systems Engineering', 
    '180000', 
    '8', 
    '1150', 
    '1', 
    '0', 
    '60', 
    '7 days',
    '1.0', 
    'unknown', 
    'Starfighter', 
    '2014-12-20T17:35:23.906000Z', 
    '2014-12-22T17:35:45.147746Z', 
    'https://swapi.co/api/starships/48/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'H-type Nubian yacht', 
    'H-type Nubian yacht', 
    'Theed Palace Space Vessel Engineering Corps', 
    'unknown', 
    '47.9', 
    '8000', 
    '4', 
    'unknown', 
    'unknown', 
    'unknown',
    '0.9', 
    'unknown', 
    'yacht', 
    '2014-12-20T17:46:46.847000Z', 
    '2014-12-22T17:35:45.158969Z', 
    'https://swapi.co/api/starships/49/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/50/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/51/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Republic Assault ship', 
    'Acclamator I-class assault ship', 
    'Rothana Heavy Engineering', 
    'unknown', 
    '752', 
    'unknown', 
    '700', 
    '16000', 
    '11250000', 
    '2 years',
    '0.6', 
    'unknown', 
    'assault ship', 
    '2014-12-20T18:08:42.926000Z', 
    '2014-12-22T17:35:45.171653Z', 
    'https://swapi.co/api/starships/52/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/53/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/54/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/55/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/56/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/57/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Solar Sailer', 
    'Punworcca 116-class interstellar sloop', 
    'Huppla Pasa Tisc Shipwrights Collective', 
    '35700', 
    '15.2', 
    '1600', 
    '3', 
    '11', 
    '240', 
    '7 days',
    '1.5', 
    'unknown', 
    'yacht', 
    '2014-12-20T18:37:56.969000Z', 
    '2014-12-22T17:35:45.183075Z', 
    'https://swapi.co/api/starships/58/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Trade Federation cruiser', 
    'Providence-class carrier/destroyer', 
    'Rendili StarDrive, Free Dac Volunteers Engineering corps.', 
    '125000000', 
    '1088', 
    '1050', 
    '600', 
    '48247', 
    '50000000', 
    '4 years',
    '1.5', 
    'unknown', 
    'capital ship', 
    '2014-12-20T19:40:21.902000Z', 
    '2014-12-22T17:35:45.195165Z', 
    'https://swapi.co/api/starships/59/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/60/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Theta-class T-2c shuttle', 
    'Theta-class T-2c shuttle', 
    'Cygnus Spaceworks', 
    '1000000', 
    '18.5', 
    '2000', 
    '5', 
    '16', 
    '50000', 
    '56 days',
    '1.0', 
    'unknown', 
    'transport', 
    '2014-12-20T19:48:40.409000Z', 
    '2014-12-22T17:35:45.208584Z', 
    'https://swapi.co/api/starships/61/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/62/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Republic attack cruiser', 
    'Senator-class Star Destroyer', 
    'Kuat Drive Yards, Allanteen Six shipyards', 
    '59000000', 
    '1137', 
    '975', 
    '7400', 
    '2000', 
    '20000000', 
    '2 years',
    '1.0', 
    'unknown', 
    'star destroyer', 
    '2014-12-20T19:52:56.232000Z', 
    '2014-12-22T17:35:45.224540Z', 
    'https://swapi.co/api/starships/63/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Naboo star skiff', 
    'J-type star skiff', 
    'Theed Palace Space Vessel Engineering Corps/Nubia Star Drives, Incorporated', 
    'unknown', 
    '29.2', 
    '1050', 
    '3', 
    '3', 
    'unknown', 
    'unknown',
    '0.5', 
    'unknown', 
    'yacht', 
    '2014-12-20T19:55:15.396000Z', 
    '2014-12-22T17:35:45.258859Z', 
    'https://swapi.co/api/starships/64/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Jedi Interceptor', 
    'Eta-2 Actis-class light interceptor', 
    'Kuat Systems Engineering', 
    '320000', 
    '5.47', 
    '1500', 
    '1', 
    '0', 
    '60', 
    '2 days',
    '1.0', 
    'unknown', 
    'starfighter', 
    '2014-12-20T19:56:57.468000Z', 
    '2014-12-22T17:35:45.272349Z', 
    'https://swapi.co/api/starships/65/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'arc-170', 
    'Aggressive Reconnaissance-170 starfighte', 
    'Incom Corporation, Subpro Corporation', 
    '155000', 
    '14.5', 
    '1000', 
    '3', 
    '0', 
    '110', 
    '5 days',
    '1.0', 
    '100', 
    'starfighter', 
    '2014-12-20T20:03:48.603000Z', 
    '2014-12-22T17:35:45.287214Z', 
    'https://swapi.co/api/starships/66/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/67/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Banking clan frigate', 
    'Munificent-class star frigate', 
    'Hoersch-Kessel Drive, Inc, Gwori Revolutionary Industries', 
    '57000000', 
    '825', 
    'unknown', 
    '200', 
    'unknown', 
    '40000000', 
    '2 years',
    '1.0', 
    'unknown', 
    'cruiser', 
    '2014-12-20T20:07:11.538000Z', 
    '2017-04-19T10:53:32.700283Z', 
    'https://swapi.co/api/starships/68/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/69/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/70/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/71/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/72/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/73/'
);



INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'Belbullab-22 starfighter', 
    'Belbullab-22 starfighter', 
    'Feethan Ottraw Scalable Assemblies', 
    '168000', 
    '6.71', 
    '1100', 
    '1', 
    '0', 
    '140', 
    '7 days',
    '6', 
    'unknown', 
    'starfighter', 
    '2014-12-20T20:38:05.031000Z', 
    '2014-12-22T17:35:45.381900Z', 
    'https://swapi.co/api/starships/74/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'V-wing', 
    'Alpha-3 Nimbus-class V-wing starfighter', 
    'Kuat Systems Engineering', 
    '102500', 
    '7.9', 
    '1050', 
    '1', 
    '0', 
    '60', 
    '15 hours',
    '1.0', 
    'unknown', 
    'starfighter', 
    '2014-12-20T20:43:04.349000Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/75/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/76/'
);

INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'T-70 X-wing fighter', 
    'T-70 X-wing fighter', 
    'Incom', 
    'unknown', 
    'unknown', 
    'unknown', 
    '1', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'fighter', 
    '2015-04-17T06:58:50.614475Z', 
    '2015-04-17T06:58:50.614528Z', 
    'https://swapi.co/api/starships/77/'
);


INSERT INTO swapi.starship
(name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew, passengers, cargo_capacity, consumables, hyperdrive_rating, mglt, starship_class, created, edited, url)
VALUES
(
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown', 
    'unknown',
    'unknown', 
    'unknown', 
    'unknown', 
    '2014-12-22T17:35:45.396711Z', 
    '2014-12-22T17:35:45.396711Z', 
    'https://swapi.co/api/starships/75/'
);
