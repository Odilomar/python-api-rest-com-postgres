INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Human', 
    'mammal', 
    'sentient', 
    '180', 
    'caucasian, black, asian, hispanic', 
    'blonde, brown, black, red', 
    'brown, blue, green, hazel, grey, amber', 
    '120', 
    'Galactic Basic', 
    '2014-12-10T13:52:11.567000Z', 
    '2015-04-17T06:59:55.850671Z', 
    'https://swapi.co/api/species/1/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Droid', 
    'artificial', 
    'sentient', 
    'n/a', 
    'n/a', 
    'n/a', 
    'n/a', 
    'indefinite', 
    'n/a', 
    '2014-12-10T15:16:16.259000Z', 
    '2015-04-17T06:59:43.869528Z', 
    'https://swapi.co/api/species/2/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Wookiee', 
    'mammal', 
    'sentient', 
    '210', 
    'gray', 
    'black, brown', 
    'blue, green, yellow, brown, golden, red', 
    '400', 
    'Shyriiwook', 
    '2014-12-10T16:44:31.486000Z', 
    '2015-01-30T21:23:03.074598Z', 
    'https://swapi.co/api/species/3/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Rodian', 
    'sentient', 
    'reptilian', 
    '170', 
    'green, blue', 
    'n/a', 
    'black', 
    'unknown', 
    'Galactic Basic', 
    '2014-12-10T17:05:26.471000Z', 
    '2016-07-19T13:27:03.156498Z', 
    'https://swapi.co/api/species/4/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Hutt', 
    'gastropod', 
    'sentient', 
    '300', 
    'green, brown, tan', 
    'n/a', 
    'yellow, red', 
    '1000', 
    'Huttese', 
    '2014-12-10T17:12:50.410000Z', 
    '2014-12-20T21:36:42.146000Z', 
    'https://swapi.co/api/species/5/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Yoda''s species', 
    'mammal', 
    'sentient', 
    '66', 
    'green, yellow', 
    'brown, white', 
    'brown, green, yellow', 
    '900', 
    'Galactic basic', 
    '2014-12-15T12:27:22.877000Z', 
    '2014-12-20T21:36:42.148000Z', 
    'https://swapi.co/api/species/6/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Trandoshan', 
    'reptile', 
    'sentient', 
    '200', 
    'brown, green', 
    'none', 
    'yellow, orange', 
    'unknown', 
    'Dosh', 
    '2014-12-15T13:07:47.704000Z', 
    '2014-12-20T21:36:42.151000Z', 
    'https://swapi.co/api/species/7/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Mon Calamari', 
    'amphibian', 
    'sentient', 
    '160', 
    'red, blue, brown, magenta', 
    'none', 
    'yellow', 
    'unknown', 
    'Mon Calamarian', 
    '2014-12-18T11:09:52.263000Z', 
    '2014-12-20T21:36:42.153000Z', 
    'https://swapi.co/api/species/8/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Ewok', 
    'mammal', 
    'sentient', 
    '100', 
    'brown', 
    'white, brown, black', 
    'orange, brown', 
    'unknown', 
    'Ewokese', 
    '2014-12-18T11:22:00.285000Z', 
    '2014-12-20T21:36:42.155000Z', 
    'https://swapi.co/api/species/9/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Sullustan', 
    'mammal', 
    'sentient', 
    '180', 
    'pale', 
    'none', 
    'black', 
    'unknown', 
    'Sullutese', 
    '2014-12-18T11:26:20.103000Z', 
    '2014-12-20T21:36:42.157000Z', 
    'https://swapi.co/api/species/10/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Neimodian', 
    'unknown', 
    'sentient', 
    '180', 
    'grey, green', 
    'none', 
    'red, pink', 
    'unknown', 
    'Neimoidia', 
    '2014-12-19T17:07:31.319000Z', 
    '2014-12-20T21:36:42.160000Z', 
    'https://swapi.co/api/species/11/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Gungan', 
    'amphibian', 
    'sentient', 
    '190', 
    'brown, green', 
    'none', 
    'orange', 
    'unknown', 
    'Gungan basic', 
    '2014-12-19T17:30:37.341000Z', 
    '2014-12-20T21:36:42.163000Z', 
    'https://swapi.co/api/species/12/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Toydarian', 
    'mammal', 
    'sentient', 
    '120', 
    'blue, green, grey', 
    'none', 
    'yellow', 
    '91', 
    'Toydarian', 
    '2014-12-19T17:48:56.893000Z', 
    '2014-12-20T21:36:42.165000Z', 
    'https://swapi.co/api/species/13/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Dug', 
    'mammal', 
    'sentient', 
    '100', 
    'brown, purple, grey, red', 
    'none', 
    'yellow, blue', 
    'unknown', 
    'Dugese', 
    '2014-12-19T17:53:11.214000Z', 
    '2014-12-20T21:36:42.167000Z', 
    'https://swapi.co/api/species/14/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Twi''lek', 
    'mammals', 
    'sentient', 
    '200', 
    'orange, yellow, blue, green, pink, purple, tan', 
    'none', 
    'blue, brown, orange, pink', 
    'unknown', 
    'Twi''leki', 
    '2014-12-20T09:48:02.406000Z', 
    '2014-12-20T21:36:42.169000Z', 
    'https://swapi.co/api/species/15/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Aleena', 
    'reptile', 
    'sentient', 
    '80', 
    'blue, gray', 
    'none', 
    'unknown', 
    '79', 
    'Aleena', 
    '2014-12-20T09:53:16.481000Z', 
    '2014-12-20T21:36:42.171000Z', 
    'https://swapi.co/api/species/16/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Vulptereen', 
    'unknown', 
    'sentient', 
    '100', 
    'grey', 
    'none', 
    'yellow', 
    'unknown', 
    'vulpterish', 
    '2014-12-20T09:57:33.128000Z', 
    '2014-12-20T21:36:42.173000Z', 
    'https://swapi.co/api/species/17/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Xexto', 
    'unknown', 
    'sentient', 
    '125', 
    'grey, yellow, purple', 
    'none', 
    'black', 
    'unknown', 
    'Xextese', 
    '2014-12-20T10:02:13.915000Z', 
    '2014-12-20T21:36:42.175000Z', 
    'https://swapi.co/api/species/18/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Toong', 
    'unknown', 
    'sentient', 
    '200', 
    'grey, green, yellow', 
    'none', 
    'orange', 
    'unknown', 
    'Tundan', 
    '2014-12-20T10:08:36.795000Z', 
    '2014-12-20T21:36:42.177000Z', 
    'https://swapi.co/api/species/19/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Cerean', 
    'mammal', 
    'sentient', 
    '200', 
    'pale pink', 
    'red, blond, black, white', 
    'hazel', 
    'unknown', 
    'Cerean', 
    '2014-12-20T10:15:33.765000Z', 
    '2014-12-20T21:36:42.179000Z', 
    'https://swapi.co/api/species/20/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Nautolan', 
    'amphibian', 
    'sentient', 
    '180', 
    'green, blue, brown, red', 
    'none', 
    'black', 
    '70', 
    'Nautila', 
    '2014-12-20T10:18:58.610000Z', 
    '2014-12-20T21:36:42.181000Z', 
    'https://swapi.co/api/species/21/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Zabrak', 
    'mammal', 
    'sentient', 
    '180', 
    'pale, brown, red, orange, yellow', 
    'black', 
    'brown, orange', 
    'unknown', 
    'Zabraki', 
    '2014-12-20T10:26:59.894000Z', 
    '2014-12-20T21:36:42.183000Z', 
    'https://swapi.co/api/species/22/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Tholothian', 
    'mammal', 
    'sentient', 
    'unknown', 
    'dark', 
    'unknown', 
    'blue, indigo', 
    'unknown', 
    'unknown', 
    '2014-12-20T10:29:13.798000Z', 
    '2014-12-20T21:36:42.186000Z', 
    'https://swapi.co/api/species/23/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Iktotchi', 
    'unknown', 
    'sentient', 
    '180', 
    'pink', 
    'none', 
    'orange', 
    'unknown', 
    'Iktotchese', 
    '2014-12-20T10:32:13.046000Z', 
    '2014-12-20T21:36:42.188000Z', 
    'https://swapi.co/api/species/24/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Quermian', 
    'mammal', 
    'sentient', 
    '240', 
    'white', 
    'none', 
    'yellow', 
    '86', 
    'Quermian', 
    '2014-12-20T10:34:50.827000Z', 
    '2014-12-20T21:36:42.189000Z', 
    'https://swapi.co/api/species/25/'
);


INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Kel Dor', 
    'unknown', 
    'sentient', 
    '180', 
    'peach, orange, red', 
    'none', 
    'black, silver', 
    '70', 
    'Kel Dor', 
    '2014-12-20T10:49:21.692000Z', 
    '2014-12-20T21:36:42.191000Z', 
    'https://swapi.co/api/species/26/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Chagrian', 
    'amphibian', 
    'sentient', 
    '190', 
    'blue', 
    'none', 
    'blue', 
    'unknown', 
    'Chagria', 
    '2014-12-20T10:53:28.795000Z', 
    '2014-12-20T21:36:42.193000Z', 
    'https://swapi.co/api/species/27/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Geonosian', 
    'insectoid', 
    'sentient', 
    '178', 
    'green, brown', 
    'none', 
    'green, hazel', 
    'unknown', 
    'Geonosian', 
    '2014-12-20T16:40:45.618000Z', 
    '2014-12-20T21:36:42.195000Z', 
    'https://swapi.co/api/species/28/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Mirialan', 
    'mammal', 
    'sentient', 
    '180', 
    'yellow, green', 
    'black, brown', 
    'blue, green, red, yellow, brown, orange', 
    'unknown', 
    'Mirialan', 
    '2014-12-20T16:46:48.290000Z', 
    '2014-12-20T21:36:42.197000Z', 
    'https://swapi.co/api/species/29/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Clawdite', 
    'reptilian', 
    'sentient', 
    '180', 
    'green, yellow', 
    'none', 
    'yellow', 
    '70', 
    'Clawdite', 
    '2014-12-20T16:57:46.171000Z', 
    '2014-12-20T21:36:42.199000Z', 
    'https://swapi.co/api/species/30/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Besalisk', 
    'amphibian', 
    'sentient', 
    '178', 
    'brown', 
    'none', 
    'yellow', 
    '75', 
    'besalisk', 
    '2014-12-20T17:28:28.821000Z', 
    '2014-12-20T21:36:42.200000Z', 
    'https://swapi.co/api/species/31/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Kaminoan', 
    'amphibian', 
    'sentient', 
    '220', 
    'grey, blue', 
    'none', 
    'black', 
    '80', 
    'Kaminoan', 
    '2014-12-20T17:31:24.838000Z', 
    '2014-12-20T21:36:42.202000Z', 
    'https://swapi.co/api/species/32/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Skakoan', 
    'mammal', 
    'sentient', 
    'unknown', 
    'grey, green', 
    'none', 
    'unknown', 
    'unknown', 
    'Skakoan', 
    '2014-12-20T17:53:54.515000Z', 
    '2014-12-20T21:36:42.204000Z', 
    'https://swapi.co/api/species/33/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Muun', 
    'mammal', 
    'sentient', 
    '190', 
    'grey, white', 
    'none', 
    'black', 
    '100', 
    'Muun', 
    '2014-12-20T17:58:19.088000Z', 
    '2014-12-20T21:36:42.207000Z', 
    'https://swapi.co/api/species/34/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Togruta', 
    'mammal', 
    'sentient', 
    '180', 
    'red, white, orange, yellow, green, blue', 
    'none', 
    'red, orange, yellow, green, blue, black', 
    '94', 
    'Togruti', 
    '2014-12-20T18:44:03.246000Z', 
    '2014-12-20T21:36:42.209000Z', 
    'https://swapi.co/api/species/35/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Kaleesh', 
    'reptile', 
    'sentient', 
    '170', 
    'brown, orange, tan', 
    'none', 
    'yellow', 
    '80', 
    'Kaleesh', 
    '2014-12-20T19:45:42.537000Z', 
    '2014-12-20T21:36:42.210000Z', 
    'https://swapi.co/api/species/36/'
);

INSERT INTO swapi.species
(name, classification, designation, average_height, skin_color, hair_color, eye_color, average_lifespan, language, created, edited, url)
VALUES
(
    'Pau''an', 
    'mammal', 
    'sentient', 
    '190', 
    'grey', 
    'none', 
    'black', 
    '700', 
    'Utapese', 
    '2014-12-20T20:35:06.777000Z', 
    '2014-12-20T21:36:42.212000Z', 
    'https://swapi.co/api/species/37/'
);

