
INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Tatooine',
    '23',
    '304',
    '10465',
    'arid',
    '1 standard',
    'desert', 
    '1',
    '200000', 
    '2014-12-09T13:50:49.641000Z', 
    '2014-12-21T20:48:04.175778Z', 
    'https://swapi.co/api/planets/1/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Alderaan',
    '24',
    '364',
    '12500',
    'temperate',
    '1 standard',
    'grasslands, mountains',
    '40',
    '2000000000',
    '2014-12-10T11:35:48.479000Z',
    '2014-12-20T20:58:18.420000Z',
    'https://swapi.co/api/planets/2/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Yavin IV',
    '24',
    '4818',
    '10200',
    'temperate, tropical',
    '1 standard',
    'jungle, rainforests',
    '8',
    '1000',
    '2014-12-10T11:37:19.144000Z',
    '2014-12-20T20:58:18.421000Z',
    'https://swapi.co/api/planets/3/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Hoth',
    '23',
    '549',
    '7200',
    'frozen',
    '1.1 standard',
    'tundra, ice caves, mountain ranges',
    '100',
    'unknown',
    '2014-12-10T11:39:13.934000Z',
    '2014-12-20T20:58:18.423000Z',
    'https://swapi.co/api/planets/4/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Dagobah',
    '23',
    '341',
    '8900',
    'murky',
    'N/A',
    'swamp, jungles',
    '8',
    'unknown',
    '2014-12-10T11:42:22.590000Z',
    '2014-12-20T20:58:18.425000Z',
    'https://swapi.co/api/planets/5/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Bespin',
    '12',
    '5110',
    '118000',
    'temperate',
    '1.5 (surface), 1 standard (Cloud City)',
    'gas giant',
    '0',
    '6000000',
    '2014-12-10T11:43:55.240000Z',
    '2014-12-20T20:58:18.427000Z',
    'https://swapi.co/api/planets/6/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Endor',
    '18',
    '402',
    '4900',
    'temperate',
    '0.85 standard',
    'forests, mountains, lakes',
    '8',
    '30000000',
    '2014-12-10T11:50:29.349000Z',
    '2014-12-20T20:58:18.429000Z',
    'https://swapi.co/api/planets/7/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Naboo',
    '26',
    '312',
    '12120',
    'temperate',
    '1 standard',
    'grassy hills, swamps, forests, mountains',
    '12',
    '4500000000',
    '2014-12-10T11:52:31.066000Z',
    '2014-12-20T20:58:18.430000Z',
    'https://swapi.co/api/planets/8/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Coruscant',
    '24',
    '368',
    '12240',
    'temperate',
    '1 standard',
    'cityscape, mountains',
    'unknown',
    '1000000000000',
    '2014-12-10T11:54:13.921000Z',
    '2014-12-20T20:58:18.432000Z',
    'https://swapi.co/api/planets/9/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Kamino',
    '27',
    '463',
    '19720',
    'temperate',
    '1 standard',
    'ocean',
    '100',
    '1000000000',
    '2014-12-10T12:45:06.577000Z',
    '2014-12-20T20:58:18.434000Z',
    'https://swapi.co/api/planets/10/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Geonosis',
    '30',
    '256',
    '11370',
    'temperate, arid',
    '0.9 standard',
    'rock, desert, mountain, barren',
    '5',
    '100000000000',
    '2014-12-10T12:47:22.350000Z',
    '2014-12-20T20:58:18.437000Z',
    'https://swapi.co/api/planets/11/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Utapau',
    '27',
    '351',
    '12900',
    'temperate, arid, windy',
    '1 standard',
    'scrublands, savanna, canyons, sinkholes',
    '0.9',
    '95000000',
    '2014-12-10T12:49:01.491000Z',
    '2014-12-20T20:58:18.439000Z',
    'https://swapi.co/api/planets/12/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Mustafar',
    '36',
    '412',
    '4200',
    'hot',
    '1 standard',
    'volcanoes, lava rivers, mountains, caves',
    '0',
    '20000',
    '2014-12-10T12:50:16.526000Z',
    '2014-12-20T20:58:18.440000Z',
    'https://swapi.co/api/planets/13/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Kashyyyk',
    '26',
    '381',
    '12765',
    'tropical',
    '1 standard',
    'jungle, forests, lakes, rivers',
    '60',
    '45000000',
    '2014-12-10T13:32:00.124000Z',
    '2014-12-20T20:58:18.442000Z',
    'https://swapi.co/api/planets/14/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Polis Massa',
    '24',
    '590',
    '0',
    'artificial temperate ',
    '0.56 standard',
    'airless asteroid',
    '0',
    '1000000',
    '2014-12-10T13:33:46.405000Z',
    '2014-12-20T20:58:18.444000Z',
    'https://swapi.co/api/planets/15/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Mygeeto',
    '12',
    '167',
    '10088',
    'frigid',
    '1 standard',
    'glaciers, mountains, ice canyons',
    'unknown',
    '19000000',
    '2014-12-10T13:43:39.139000Z',
    '2014-12-20T20:58:18.446000Z',
    'https://swapi.co/api/planets/16/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Felucia',
    '34',
    '231',
    '9100',
    'hot, humid',
    '0.75 standard',
    'fungus forests',
    'unknown',
    '8500000',
    '2014-12-10T13:44:50.397000Z',
    '2014-12-20T20:58:18.447000Z',
    'https://swapi.co/api/planets/17/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Cato Neimoidia',
    '25',
    '278',
    '0',
    'temperate, moist',
    '1 standard',
    'mountains, fields, forests, rock arches', 
    'unknown',
    '10000000', 
    '2014-12-10T13:46:28.704000Z', 
    '2014-12-20T20:58:18.449000Z', 
    'https://swapi.co/api/planets/18/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Saleucami',
    '26',
    '392',
    '14920',
    'hot',
    'unknown',
    'caves, desert, mountains, volcanoes', 
    'unknown',
    '1400000000', 
    '2014-12-10T13:47:46.874000Z', 
    '2014-12-20T20:58:18.450000Z', 
    'https://swapi.co/api/planets/19/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Stewjon',
    'unknown',
    'unknown',
    '0',
    'temperate',
    '1 standard',
    'grass', 
    'unknown',
    'unknown', 
    '2014-12-10T16:16:26.566000Z', 
    '2014-12-20T20:58:18.452000Z', 
    'https://swapi.co/api/planets/20/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Eriadu',
    '24',
    '360',
    '13490',
    'polluted',
    '1 standard',
    'cityscape', 
    'unknown',
    '22000000000', 
    '2014-12-10T16:26:54.384000Z', 
    '2014-12-20T20:58:18.454000Z', 
    'https://swapi.co/api/planets/21/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Corellia',
    '25',
    '329',
    '11000',
    'temperate',
    '1 standard',
    'plains, urban, hills, forests', 
    '70',
    '3000000000', 
    '2014-12-10T16:49:12.453000Z', 
    '2014-12-20T20:58:18.456000Z', 
    'https://swapi.co/api/planets/22/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Rodia',
    '29',
    '305',
    '7549',
    'hot',
    '1 standard',
    'jungles, oceans, urban, swamps', 
    '60',
    '1300000000', 
    '2014-12-10T17:03:28.110000Z', 
    '2014-12-20T20:58:18.458000Z', 
    'https://swapi.co/api/planets/23/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Nal Hutta',
    '87',
    '413',
    '12150',
    'temperate',
    '1 standard',
    'urban, oceans, swamps, bogs', 
    'unknown',
    '7000000000', 
    '2014-12-10T17:11:29.452000Z', 
    '2014-12-20T20:58:18.460000Z', 
    'https://swapi.co/api/planets/24/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Dantooine',
    '25',
    '378',
    '9830',
    'temperate',
    '1 standard',
    'oceans, savannas, mountains, grasslands', 
    'unknown',
    '1000', 
    '2014-12-10T17:23:29.896000Z', 
    '2014-12-20T20:58:18.461000Z', 
    'https://swapi.co/api/planets/25/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Bestine IV',
    '26',
    '680',
    '6400',
    'temperate',
    'unknown',
    'rocky islands, oceans', 
    '98',
    '62000000', 
    '2014-12-12T11:16:55.078000Z', 
    '2014-12-20T20:58:18.463000Z', 
    'https://swapi.co/api/planets/26/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Ord Mantell',
    '26',
    '334',
    '14050',
    'temperate',
    '1 standard',
    'plains, seas, mesas', 
    '10',
    '4000000000', 
    '2014-12-15T12:23:41.661000Z', 
    '2014-12-20T20:58:18.464000Z', 
    'https://swapi.co/api/planets/27/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'unknown',
    '0',
    '0',
    '0',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    '2014-12-15T12:25:59.569000Z', 
    '2014-12-20T20:58:18.466000Z', 
    'https://swapi.co/api/planets/28/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Trandosha',
    '25',
    '371',
    '0',
    'arid',
    '0.62 standard',
    'mountains, seas, grasslands, deserts', 
    'unknown',
    '42000000', 
    '2014-12-15T12:53:47.695000Z', 
    '2014-12-20T20:58:18.468000Z', 
    'https://swapi.co/api/planets/29/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Socorro',
    '20',
    '326',
    '0',
    'arid',
    '1 standard',
    'deserts, mountains', 
    'unknown',
    '300000000', 
    '2014-12-15T12:56:31.121000Z', 
    '2014-12-20T20:58:18.469000Z', 
    'https://swapi.co/api/planets/30/'
);


INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Mon Cala',
    '21',
    '398',
    '11030',
    'temperate',
    '1',
    'oceans, reefs, islands', 
    '100',
    '27000000000', 
    '2014-12-18T11:07:01.792000Z', 
    '2014-12-20T20:58:18.471000Z', 
    'https://swapi.co/api/planets/31/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Chandrila',
    '20',
    '368',
    '13500',
    'temperate',
    '1',
    'plains, forests', 
    '40',
    '1200000000', 
    '2014-12-18T11:11:51.872000Z', 
    '2014-12-20T20:58:18.472000Z', 
    'https://swapi.co/api/planets/32/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Sullust',
    '20',
    '263',
    '12780',
    'superheated',
    '1',
    'mountains, volcanoes, rocky deserts', 
    '5',
    '18500000000', 
    '2014-12-18T11:25:40.243000Z', 
    '2014-12-20T20:58:18.474000Z', 
    'https://swapi.co/api/planets/33/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Toydaria',
    '21',
    '184',
    '7900',
    'temperate',
    '1',
    'swamps, lakes', 
    'unknown',
    '11000000', 
    '2014-12-19T17:47:54.403000Z', 
    '2014-12-20T20:58:18.476000Z', 
    'https://swapi.co/api/planets/34/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Malastare',
    '26',
    '201',
    '18880',
    'arid, temperate, tropical',
    '1.56',
    'swamps, deserts, jungles, mountains', 
    'unknown',
    '2000000000', 
    '2014-12-19T17:52:13.106000Z', 
    '2014-12-20T20:58:18.478000Z', 
    'https://swapi.co/api/planets/35/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Dathomir',
    '24',
    '491',
    '10480',
    'temperate',
    '0.9',
    'forests, deserts, savannas', 
    'unknown',
    '5200', 
    '2014-12-19T18:00:40.142000Z', 
    '2014-12-20T20:58:18.480000Z', 
    'https://swapi.co/api/planets/36/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Ryloth',
    '30',
    '305',
    '10600',
    'temperate, arid, subartic',
    '1',
    'mountains, valleys, deserts, tundra', 
    '5',
    '1500000000', 
    '2014-12-20T09:46:25.740000Z', 
    '2014-12-20T20:58:18.481000Z', 
    'https://swapi.co/api/planets/37/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Aleen Minor',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown', 
    'unknown',
    'unknown', 
    '2014-12-20T09:52:23.452000Z', 
    '2014-12-20T20:58:18.483000Z', 
    'https://swapi.co/api/planets/38/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Vulpter',
    '22',
    '391',
    '14900',
    'temperate, artic',
    '1',
    'urban, barren', 
    'unknown',
    '421000000', 
    '2014-12-20T09:56:58.874000Z', 
    '2014-12-20T20:58:18.485000Z', 
    'https://swapi.co/api/planets/39/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Troiken',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'desert, tundra, rainforests, mountains', 
    'unknown',
    'unknown', 
    '2014-12-20T10:01:37.395000Z', 
    '2014-12-20T20:58:18.487000Z', 
    'https://swapi.co/api/planets/40/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Tund',
    '48',
    '1770',
    '12190',
    'unknown',
    'unknown',
    'barren, ash', 
    'unknown',
    '0', 
    '2014-12-20T10:07:29.578000Z', 
    '2014-12-20T20:58:18.489000Z', 
    'https://swapi.co/api/planets/41/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Haruun Kal',
    '25',
    '383',
    '10120',
    'temperate',
    '0.98',
    'toxic cloudsea, plateaus, volcanoes', 
    'unknown',
    '705300', 
    '2014-12-20T10:12:28.980000Z', 
    '2014-12-20T20:58:18.491000Z', 
    'https://swapi.co/api/planets/42/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Cerea',
    '27',
    '386',
    'unknown',
    'temperate',
    '1',
    'verdant', 
    '20',
    '450000000', 
    '2014-12-20T10:14:48.178000Z', 
    '2014-12-20T20:58:18.493000Z', 
    'https://swapi.co/api/planets/43/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Glee Anselm',
    '33',
    '206',
    '15600',
    'tropical, temperate',
    '1',
    'lakes, islands, swamps, seas', 
    '80',
    '500000000', 
    '2014-12-20T10:18:26.110000Z', 
    '2014-12-20T20:58:18.495000Z', 
    'https://swapi.co/api/planets/44/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Iridonia',
    '29',
    '413',
    'unknown',
    'unknown',
    'unknown',
    'rocky canyons, acid pools', 
    'unknown',
    'unknown', 
    '2014-12-20T10:26:05.788000Z', 
    '2014-12-20T20:58:18.497000Z', 
    'https://swapi.co/api/planets/45/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Tholoth',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown', 
    'unknown',
    'unknown', 
    '2014-12-20T10:28:31.117000Z', 
    '2014-12-20T20:58:18.498000Z', 
    'https://swapi.co/api/planets/46/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Iktotch',
    '22',
    '481',
    'unknown',
    'arid, rocky, windy',
    '1',
    'rocky', 
    'unknown',
    'unknown', 
    '2014-12-20T10:31:32.413000Z', 
    '2014-12-20T20:58:18.500000Z', 
    'https://swapi.co/api/planets/47/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Quermia',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown', 
    'unknown',
    'unknown', 
    '2014-12-20T10:34:08.249000Z', 
    '2014-12-20T20:58:18.502000Z', 
    'https://swapi.co/api/planets/48/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Dorin',
    '22',
    '409',
    '13400',
    'temperate',
    '1',
    'unknown', 
    'unknown',
    'unknown', 
    '2014-12-20T10:48:36.141000Z', 
    '2014-12-20T20:58:18.504000Z', 
    'https://swapi.co/api/planets/49/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Champala',
    '27',
    '318',
    'unknown',
    'temperate',
    '1',
    'oceans, rainforests, plateaus', 
    'unknown',
    '3500000000', 
    '2014-12-20T10:52:51.524000Z', 
    '2014-12-20T20:58:18.506000Z', 
    'https://swapi.co/api/planets/50/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Mirial',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'deserts', 
    'unknown',
    'unknown', 
    '2014-12-20T16:44:46.318000Z', 
    '2014-12-20T20:58:18.508000Z', 
    'https://swapi.co/api/planets/51/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Serenno',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'rainforests, rivers, mountains', 
    'unknown',
    'unknown', 
    '2014-12-20T16:52:13.357000Z', 
    '2014-12-20T20:58:18.510000Z', 
    'https://swapi.co/api/planets/52/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Concord Dawn',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'jungles, forests, deserts', 
    'unknown',
    'unknown', 
    '2014-12-20T16:54:39.909000Z', 
    '2014-12-20T20:58:18.512000Z', 
    'https://swapi.co/api/planets/53/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Zolan',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown', 
    'unknown',
    'unknown', 
    '2014-12-20T16:56:37.250000Z', 
    '2014-12-20T20:58:18.514000Z', 
    'https://swapi.co/api/planets/54/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Ojom',
    'unknown',
    'unknown',
    'unknown',
    'frigid',
    'unknown',
    'oceans, glaciers', 
    '100',
    '500000000', 
    '2014-12-20T17:27:41.286000Z', 
    '2014-12-20T20:58:18.516000Z', 
    'https://swapi.co/api/planets/55/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Skako',
    '27',
    '384',
    'unknown',
    'temperate',
    '1',
    'urban, vines', 
    'unknown',
    '500000000000', 
    '2014-12-20T17:50:47.864000Z', 
    '2014-12-20T20:58:18.517000Z', 
    'https://swapi.co/api/planets/56/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Muunilinst',
    '28',
    '412',
    '13800',
    'temperate',
    '1',
    'plains, forests, hills, mountains', 
    '25',
    '5000000000', 
    '2014-12-20T17:57:47.420000Z', 
    '2014-12-20T20:58:18.519000Z', 
    'https://swapi.co/api/planets/57/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Shili',
    'unknown',
    'unknown',
    'unknown',
    'temperate',
    '1',
    'cities, savannahs, seas, plains', 
    'unknown',
    'unknown', 
    '2014-12-20T18:43:14.049000Z', 
    '2014-12-20T20:58:18.521000Z', 
    'https://swapi.co/api/planets/58/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Kalee',
    '23',
    '378',
    '13850',
    'arid, temperate, tropical',
    '1',
    'rainforests, cliffs, canyons, seas', 
    'unknown',
    '4000000000', 
    '2014-12-20T19:43:51.278000Z', 
    '2014-12-20T20:58:18.523000Z', 
    'https://swapi.co/api/planets/59/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Umbara',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown', 
    'unknown',
    'unknown', 
    '2014-12-20T20:18:36.256000Z', 
    '2014-12-20T20:58:18.525000Z', 
    'https://swapi.co/api/planets/60/'
);

INSERT INTO swapi.planet
(name, rotation_period, orbital_period, diameter, climate, gravity, terrain, surface_water, population, created, edited, url)
VALUES (
    'Jakku',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'unknown',
    'deserts', 
    'unknown',
    'unknown', 
    '2015-04-17T06:55:57.556495Z', 
    '2015-04-17T06:55:57.556551Z', 
    'https://swapi.co/api/planets/61/'
);