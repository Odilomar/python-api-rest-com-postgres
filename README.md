# STAR WARS RESTFULL API - FLASK + POSTGRES

Esse projeto é um serviço criado em Python usando o framework FLASK com o banco de dados em POSTGRESQL.

## Instalação
1. Baixe o SGBD. [Banco de dados: Postgres @ v12.1](https://www.postgresql.org/download/)
2. Baixe os pacotes da Linguage. [Python @ v3.8.1](https://www.python.org/downloads/)
    * Durante a instalação do Python, marcar a opção PIP.
3. Instale **Flask** @ v1.1.1: 
```
    pip install flask
```
4. Instale **FLASK CORS** @ v3.0.8:
```
    pip install -U flask-cors
```
5. **Psycopg2** @ v2.8.4:
```
    pip install psycopg2
```
6. Clone o repositório
```
    git clone https://gitlab.com/Odilomar/python-api-rest-com-postgres.git
```

## Banco de dados
No banco de dados, crie uma instância do banco usando o seguinte comando:
```
    CREATE DATABASE swapi
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
```
Depois crie um **SCHEMA** chamado SWAPI:
```
    CREATE SCHEMA swapi AUTHORIZATION postgres;
```
Em seguida, procure o arquivo **'assets/database/[LOCALHOST] FULL DATABASE.txt**' e o execute. Este arquivo irá criar as estruturas das tabelas, preenchirá as mesmas e criará as funções do banco de dados necessárias pela API.

## API
Abra o arquivo server.py e modifique **Connection String**.
```
    postgresql://usuario_do_banco:senha_do_banco@localhost:5432/swapi
```

Abra o **CMD** na pasta do projeto e digite:
```
    python server.py run
```

Em seguida, o serviço estará rodando e poderá ser consumido localmente no endereço:
```
    http://localhost:80/
```

A porta pode ser modificada no arquivo **server.py**. Ela é passada como parâmetro na função **run**.

### Rotas
Segue a listagem de rotas da API:
1. GET: `/`
    * Retorna o nome da API
2. GET: `/version`
    * Retorna a versão da API
3. GET: `/categorypage`
    * Retorna as categorias existentes na API
4. GET: `/films/`
    * Retorna a primeira página que contém uma lista de dez filmes. 
    * No caso de filmes, Ele tem apenas uma única página.
5. GET: `/films/<int:idfilm>/`
    * Retorna as informações de um filme cujo id foi passado como parâmetro
6. GET: `/people/`
    * Retorna a primeira página que contém uma lista de dez pessoas. 
    * É possível passar como parâmetro uma página específica: '/people/?page=2'.
    * Há um total de 9 página disponíveis.
7. GET: `/people/<int:idpeople>/`
    * Retorna as informações de uma pessoa cujo id foi passado como parâmetro.
8. GET: `/planets/`
    * Retorna a primeira página que contém uma lista de dez planetas. 
    * É possível passar como parâmetro uma página específica: '/planets/?page=2'.
    * Há um total de 7 páginas disponíveis.
9. GET: `/planets/<int:idplanets>/`
    * Retorna as informações de um planeta cujo id foi passado como parâmetro
10. GET: `/species/`
    * Retorna a primeira página que contém uma lista de dez espécies. 
    * É possível passar como parâmetro uma página específica: '/species/?page=2'.
    * Há um total de 4 páginas disponíveis.
11. GET: `/species/<int:idspecies>/`
    * Retorna as informações de uma espécie cujo id foi passado como parâmetro
12. GET: `/starships/`
    * Retorna a primeira página que contém uma lista de dez naves. 
    * É possível passar como parâmetro uma página específica: '/starships/?page=2'.
    * Há um total de 4 páginas disponíveis.
13. GET: `/starships/<int:idstarships>/`
    * Retorna as informações de uma nave cujo id foi passado como parâmetro
14. GET: `/vehicles/`
    * Retorna a primeira página que contém uma lista de dez veículos. 
    * É possível passar como parâmetro uma página específica: '/vehicles/?page=2'.
    * Há um total de 4 páginas disponíveis.
15. GET: `/vehicles/<int:idvehicles>/`
    * Retorna as informações de um veículo cujo id foi passado como parâmetro
